#!/usr/bin/env python
"""Parser to determine prescription end dates.

Phil Weber 2017-03-09 Basic stats and parsing of the PolyPharmacy data.

Require: to be able to determine how long the prescription will last.

Assumptions: (possibly incomplete).
  o  Ref abbreviations https://en.wikipedia.org/wiki/List_of_abbreviations_used_in_medical_prescriptions
  o  Assume longest duration of prescription where there is an option,
     e.g. "one or two" becomes one
  o  asd1p stands for "as directed 1 per day"
  o  all qty unit "gram" (cream, powder, "apply", "as dir"), "device" excluded, don't know how much per application; so how long they would last.
     (subdermal implant - how long should it last?)
  o  drop/spray with ml/gram we cannot parse - how big is it?  Dose/unit dose OK.
  o  anything with "as directed" or equivalent is excluded, overriding any other text (could be relaxed).
  o  NDQ emollient etc. excluded.

Examples of exclusions:
  1.  multiples: dose = "1CAPSULE MANE  AND 2 EVENING", "in the morning and ..."
  2.  multiples: dose = 2am , 2 evening ... 2am, 2 pm ... 2every ... 2puffs ... 2tablets
  3.  multiples: dose = 2 twice a day and 1 noon"
  4.  increasing/decreasing: dose = "increase by" / "reducing dose"
  5.  increasing: dose = "EVERY NIGHT FOR 2 WEEKS THEN TWICE A WEEK FOR 3 MONTHS"
  6.  fixed terms: maypole: fixed terms, e.g. "from nov 14 for 12 months" (extra spaces)
  7.  alternates: e.g. "(20mg/40mg alternate days)": assumes first (20mg).

To do:
  1.  Grammar has evolved: simplify.
  2   Deal with end dates, e.g. "until March 2021", "for 12 weeks"
  3   Deal with change, e.g. "increase dose"
  4   Check for false positive matches.
  5   Deal better with typos e.g. spelling via Levenshtein distance?
  6   Probably problems with multiple prescriptions on same day - should be seq. not pll (cf Ciaron above)
"""

import sys
import csv
import re
import argparse
import time
import pyparsing as pp

DISP_INJ = "pre-filled disposable injection"


def parse_poss_frac(x):
    """Parse a value which may be an int, string, or string fraction."""
    x = x[0]  # pyparsing results: a type of list
    if isinstance(x, str):
        if '/' in x:
            xx = x.split('/')
            xxx = float(xx[0]) / float(xx[1])
        else:
            xxx = float(x)
    elif isinstance(x, int):
        xxx = x
    else:
        xxx = float(''.join(x))

    return xxx


class Parser(object):
    """Top-level Parser -- rules to inherit.

    This has evolved organically and could do with re-writing cleanly.
    """
    vstring = pp.Word(pp.alphas + '-')
    vstring_sp = pp.Word(pp.alphas + '-' + ' ')

    a = pp.Optional('a')

    spaces = pp.Word(' ')

    number = pp.Word(pp.nums) \
             + pp.Optional(pp.Suppress(pp.Literal(',')) + pp.Word(pp.nums)) \
             + pp.Optional(pp.Literal('.') + pp.Word(pp.nums))  # [0-9]*{\.[0-9]*}

    # Removed optional spaces here and also optional around 's'
    plural = pp.Optional(
        pp.Optional('(') \
        + pp.Optional(pp.CaselessLiteral('e')) + pp.CaselessLiteral('s') \
        + pp.Optional(')')
    )

    # Numbers - this could be made generic
    one = (pp.Keyword('1')
           | pp.CaselessLiteral("one")
           | pp.CaselessKeyword("i")
           | pp.CaselessKeyword("a")
           | pp.CaselessLiteral("0ne")
           ).setParseAction(pp.replaceWith('1'))
    two = (pp.Keyword('2') | pp.CaselessLiteral("two")).setParseAction(pp.replaceWith('2'))
    three = (pp.Keyword('3') | pp.CaselessLiteral("three")).setParseAction(pp.replaceWith('3'))
    four = (pp.Keyword('4') | pp.CaselessLiteral("four")).setParseAction(pp.replaceWith('4'))
    five = (pp.Keyword('5') | pp.CaselessLiteral("five")).setParseAction(pp.replaceWith('5'))
    six = (pp.Keyword('6') | pp.CaselessLiteral("six")).setParseAction(pp.replaceWith('6'))
    seven = (pp.Keyword('7') | pp.CaselessLiteral("seven")).setParseAction(pp.replaceWith('7'))
    eight = (pp.Keyword('8') | pp.CaselessLiteral("eight")).setParseAction(pp.replaceWith('8'))
    nine = (pp.Keyword('9') | pp.CaselessLiteral("nine")).setParseAction(pp.replaceWith('9'))
    ten = (pp.Keyword('10') | pp.CaselessLiteral("ten")).setParseAction(pp.replaceWith('10'))
    twenty = (pp.Keyword('20') | pp.CaselessLiteral("twenty")).setParseAction(pp.replaceWith('20'))

    once = pp.CaselessLiteral("once").setParseAction(pp.replaceWith('1'))
    twice = (pp.CaselessLiteral("twice") | pp.CaselessLiteral("bd")).setParseAction(pp.replaceWith('2'))

    digit = one | two | three | four | five | six | seven | eight | nine | ten \
            | once | twice

    # Confirm if want to suppress all of these
    unitmed = pp.Optional((pp.CaselessLiteral("tablet") + plural)
                          | (pp.CaselessLiteral("tab") + plural)
                          | (pp.CaselessLiteral("caplet") + plural)
                          | (pp.CaselessLiteral("spray") + plural)
                          | (pp.CaselessLiteral("puff") + plural)
                          | (pp.CaselessLiteral("injection") + plural)
                          | (pp.CaselessLiteral("inhalation") + plural)
                          | (pp.CaselessLiteral("patch") + plural)
                          | (pp.CaselessLiteral("drop") + plural)
                          | (pp.CaselessLiteral("a mil") + plural)
                          | (pp.CaselessLiteral("capsule") + plural)
                          | (pp.CaselessLiteral("spoonful") + plural)
                          | (pp.CaselessLiteral("suppository") + plural)
                          | (pp.CaselessLiteral("dose") + plural)
                          | (pp.CaselessLiteral("sachet") + plural)).setResultsName("unitmed")

    unitcream = pp.Suppress(pp.Optional(pp.CaselessLiteral("cream")
                                        | pp.Optional(pp.CaselessLiteral("ointment")
                                                      | pp.Optional(pp.CaselessLiteral("gel")
                                                                    ))))


class ParserNDQ(Parser):
    """Parser for PolyPharmacy "Name, Dosage and Quantity" field.

    Mainly "Drug Amount [detail] Unit" e.g. "Bisoprolol 1.25mg tablets"
    More complex e.g. for epipens.
    Details TBC.
    """
    drug = Parser.vstring
    drug_modifier = pp.Suppress(Parser.vstring_sp)

    wtspec = pp.CaselessLiteral("mg") | "%" \
             | ((pp.CaselessLiteral("microgram") | pp.CaselessLiteral("unit")) + Parser.plural)
    denspec = pp.CaselessLiteral("mg/ml")  # for epipens etc.
    howmuch = Parser.number + pp.Optional(Parser.spaces) + pp.Suppress(wtspec)
    density = Parser.number + pp.Optional(Parser.spaces) + pp.Suppress(denspec)

    # For epipens etc.
    volspec = pp.CaselessLiteral("ml")
    totalvol = Parser.number + pp.Optional(Parser.spaces) \
               + pp.Suppress(volspec) + pp.Suppress(pp.CaselessLiteral("pre-filled"))

    NDQ_std = drug.setResultsName("drug") \
              + pp.Optional(drug_modifier).setResultsName("mod") \
              + howmuch.setResultsName("howmuch") \
              + pp.Optional(drug_modifier).setResultsName("mod") \
              + Parser.unitmed.setResultsName("unit")

    NDQ_pen = drug.setResultsName("drug") \
              + pp.Optional(drug_modifier).setResultsName("mod") \
              + density.setResultsName("howmuch") \
              + pp.Optional(drug_modifier).setResultsName("mod") \
              + pp.Optional(totalvol).setResultsName("totalvol") \
              + Parser.unitmed.setResultsName("unit")

    NDQ = NDQ_pen | NDQ_std

    def __init__(self):
        """Initialise grammar."""
        pass

    def ndqparse(self, exp):
        """Parse all NDQ specifications."""
        try:
            results = self.NDQ.parseString(exp)
            return results, True
        except:
            return exp, False


class ParserDose(Parser):
    """Parser for PolyPharmacy DOSE field.

    Details TBC.
    """
    # Ranges - Could make this more generic.
    range05_1 = pp.CaselessLiteral("either") + pp.Literal("1/2") + pp.CaselessLiteral("or") + Parser.one
    range1_2 = Parser.one + pp.Optional(Parser.spaces) \
               + (pp.Literal('-') | pp.CaselessLiteral("or") | pp.CaselessLiteral("to")) \
               + pp.Optional(Parser.spaces) + Parser.two
    range2_3 = Parser.two + pp.Optional(Parser.spaces) \
               + (pp.Literal('-') | pp.CaselessLiteral("or") | pp.CaselessLiteral("to")) \
               + pp.Optional(Parser.spaces) + Parser.three
    range3_4 = Parser.three + pp.Optional(Parser.spaces) \
               + (pp.Literal('-') | pp.CaselessLiteral("or") | pp.CaselessLiteral("to")) \
               + pp.Optional(Parser.spaces) + Parser.four
    range4_6 = Parser.four + pp.Optional(Parser.spaces) \
               + (pp.Literal('-') | pp.CaselessLiteral("or") | pp.CaselessLiteral("to")) \
               + pp.Optional(Parser.spaces) + Parser.six
    range5_10 = Parser.five + pp.Optional(Parser.spaces) \
                + (pp.Literal('-') | pp.CaselessLiteral("or") | pp.CaselessLiteral("to")) \
                + pp.Optional(Parser.spaces) + Parser.ten
    range10_20 = Parser.ten + pp.Optional(Parser.spaces) \
                 + (pp.Literal('-') | pp.CaselessLiteral("or") | pp.CaselessLiteral("to")) \
                 + pp.Optional(Parser.spaces) + Parser.twenty

    # x ml - y ml --> x
    volrange = Parser.number + pp.Optional(Parser.spaces) \
               + pp.Suppress(
                    pp.Optional(pp.CaselessLiteral("ml")) + pp.Optional(Parser.spaces) \
                  + ('-' | pp.CaselessLiteral("to") | pp.CaselessLiteral("or"))
                  + Parser.number + pp.Optional(Parser.spaces)
                  + pp.Optional(pp.CaselessLiteral("ml")))

    # Could make more generic.
    fraction = (pp.Optional(pp.CaselessLiteral("one"))
                + pp.CaselessLiteral("half") + pp.Optional(pp.CaselessKeyword('a'))
                ).setParseAction(pp.replaceWith("0.5"))

    # Assume 1 if missing
    vaguehowmanyoften = pp.Suppress(pp.Optional(pp.CaselessKeyword('a'))) + (
        range05_1.setParseAction(pp.replaceWith("0.5"))
        | range1_2.setParseAction(pp.replaceWith("1"))
        | range2_3.setParseAction(pp.replaceWith("2"))
        | range3_4.setParseAction(pp.replaceWith("3"))
        | range4_6.setParseAction(pp.replaceWith("4"))
        | range5_10.setParseAction(pp.replaceWith("5"))
    )
    howmany = vaguehowmanyoften | fraction | Parser.digit

    vague = pp.Optional(((pp.CaselessLiteral("up") + pp.Optional(pp.CaselessLiteral("to")))
                         | pp.CaselessLiteral("max")
                         ).setParseAction(pp.replaceWith("max")))

    howoften = (vaguehowmanyoften | Parser.digit).setResultsName("freq")

    syrhowmuch = Parser.number + pp.Suppress(pp.CaselessLiteral("ml") + pp.Optional(Parser.plural))

    volhowmuch1 = (pp.CaselessLiteral("half a mil") + pp.Optional(pp.Literal("(0.5ml)"))).setParseAction(
        pp.replaceWith("0.5"))
    volhowmuch2 = pp.Literal("(0.25ml)").setParseAction(pp.replaceWith("0.25"))
    volhowmuch3 = ((Parser.number | range5_10) + pp.Suppress(pp.Optional(Parser.spaces)) \
                   + pp.Suppress(pp.CaselessLiteral("ml")))

    volhowmuch4a = pp.Group(pp.CaselessLiteral("1 teaspoon")
                            + pp.Optional(pp.CaselessLiteral("ful")) + pp.Optional(Parser.plural)).setParseAction(
        pp.replaceWith('5'))
    volhowmuch4b = pp.Group(pp.CaselessLiteral("2 teaspoon")
                            + pp.Optional(pp.CaselessLiteral("ful")) + pp.Optional(Parser.plural)).setParseAction(
        pp.replaceWith('10'))
    volhowmuch5 = Parser.number + pp.Suppress(pp.CaselessLiteral('x') + Parser.number)
    volhowmuch = pp.Group(volrange | volhowmuch1 | volhowmuch2 | volhowmuch3 | volhowmuch4a | volhowmuch4b
                          | volhowmuch5)

    wthowmuch = Parser.number \
                + pp.Suppress(pp.CaselessLiteral("mg") | pp.CaselessLiteral("microgram") + Parser.plural)

    # Replace timespec with numeric "how many doses per day"
    immediate = pp.CaselessLiteral("im").setParseAction(pp.replaceWith(1))  # Assume 1 day now
    daily = (pp.Literal("a day")
             | pp.Literal("aday")
             | pp.Literal("day")
             | pp.Literal("per day")
             | pp.Literal("each day")
             | pp.Literal("every day")
             | pp.Literal("during the day")
             | pp.Literal("od")
             | pp.Literal("o.d.")
             | pp.Keyword("on")
             | pp.Keyword("o.n.")
             | pp.Literal("daily")
             | pp.Keyword("d")
             | pp.Keyword("1d")
             | pp.Literal("at night")
             | pp.Literal("night")
             | pp.Literal("each night")
             | pp.Literal("every night")
             | pp.Literal("mane")
             | (pp.Literal("noct") + pp.Optional(pp.Literal("e")))
             | pp.Literal("asd1p")
             | pp.Literal("nocturnally")
             | pp.Literal("in the morning")
             | pp.Literal("each morning")
             | pp.Literal("every morning")
             | pp.Literal("with evening meal")
             ).setParseAction(pp.replaceWith('1'))
    bidaily = (pp.CaselessLiteral("bd") | pp.CaselessLiteral("bd/prn") | pp.CaselessLiteral("morning and night")
               ).setParseAction(pp.replaceWith('2'))
    tridaily = pp.CaselessLiteral("tds").setParseAction(pp.replaceWith('3'))  # numeric
    quadaily = (pp.CaselessLiteral("after meals and at bedtime")
                | pp.CaselessLiteral("qds")).setParseAction(pp.replaceWith("4"))
    twodaily1 = ((pp.CaselessLiteral("alternative") | pp.CaselessLiteral("alternate"))
                 + pp.Optional(pp.CaselessLiteral("night") | pp.CaselessLiteral("days"))
                 )
    twodaily2 = pp.CaselessLiteral("every other day")
    twodaily = (twodaily1 | twodaily2).setParseAction(pp.replaceWith("0.5"))
    threedaily = pp.CaselessLiteral("72 hours").setParseAction(pp.replaceWith("3/7"))
    weekly = (pp.CaselessLiteral("each week")
              | pp.CaselessLiteral("weekly")
              | pp.CaselessLiteral("per week")
              | pp.CaselessLiteral("each week")
              | pp.CaselessLiteral("every week")
              | pp.CaselessLiteral("a week")
              | pp.CaselessLiteral("every 7 days")
              | pp.CaselessLiteral("7 days")
              ).setParseAction(pp.replaceWith("1/7"))
    biweekly = pp.CaselessLiteral("twice a week").setParseAction((pp.replaceWith("2/7")))
    monthly = (pp.CaselessLiteral("monthly") | pp.CaselessLiteral("month")).setParseAction(pp.replaceWith("1/28"))

    # these are rather specific
    twoweekly = pp.CaselessLiteral("every 2 weeks").setParseAction(pp.replaceWith("1/14"))
    twelveweekly = pp.Suppress(pp.Optional(pp.CaselessLiteral("every"))) \
                   + (pp.CaselessLiteral("12") + pp.Optional(Parser.spaces) + pp.CaselessLiteral("w") + pp.Optional(
        pp.CaselessLiteral("ks"))).setParseAction(pp.replaceWith("1/84"))
    threemonthly = (pp.CaselessLiteral("every 3") + pp.Optional(Parser.spaces)
                    + (pp.CaselessLiteral("months") | pp.CaselessLiteral("/12"))).setParseAction(pp.replaceWith("1/91"))
    halfyearly = (pp.CaselessLiteral("every 6") + pp.Optional(Parser.spaces)
                  + (pp.CaselessLiteral("m") | pp.CaselessLiteral("mon") | pp.CaselessLiteral("months"))
                  ).setParseAction(pp.replaceWith("1/182"))

    # Could be more generic
    freqspec = pp.CaselessLiteral("every 4-6 hours up to four times a day").setParseAction(pp.replaceWith("4"))

    timespec = (pp.Optional(pp.Suppress(pp.Literal("times")))
                + (immediate | daily | bidaily | tridaily | quadaily | twodaily | threedaily | weekly | biweekly |
                   monthly | twoweekly | twelveweekly | threemonthly | halfyearly | freqspec)
                ).setResultsName("dose_per_day")

    direction_post = pp.Suppress((pp.CaselessLiteral("to be taken")
                                  | pp.CaselessLiteral("to be used")
                                  | pp.CaselessLiteral("insert")
                                  | pp.CaselessLiteral("to use")
                                  | pp.CaselessLiteral("use")
                                  | pp.CaselessLiteral("to be inhaled")
                                  | pp.CaselessLiteral("subcutaneous")
                                  | (pp.CaselessLiteral("apply") | pp.CaselessKeyword("applt"))
                                  | pp.CaselessLiteral("to be applied")
                                  | pp.CaselessLiteral("to be inserted")
                                  | pp.CaselessKeyword("a"))  # to get 'a' on its own (short for apply)
                                 + pp.Optional(pp.CaselessLiteral("in each nostril")))

    direction_pre = pp.Suppress(pp.CaselessLiteral("take")
                                | pp.CaselessLiteral("to take")
                                | pp.CaselessLiteral("put")
                                | pp.CaselessLiteral("to be used")
                                | pp.CaselessLiteral("to be inserted")
                                | pp.CaselessLiteral("to be taken")
                                | pp.CaselessLiteral("inhaler")
                                | pp.CaselessLiteral("place")
                                | pp.CaselessLiteral("insert")
                                | pp.CaselessLiteral("to use")
                                | pp.CaselessLiteral("use")
                                | pp.CaselessKeyword("add")
                                | pp.CaselessLiteral("spray")
                                | pp.CaselessLiteral("dissolve or swallow")
                                | pp.CaselessLiteral("swallow or dissolve")
                                | pp.CaselessLiteral("dissolve")
                                | pp.CaselessLiteral("into mouth")
                                | pp.CaselessLiteral("rinse mouth with")
                                | pp.CaselessLiteral("dissolve contents of")
                                | pp.CaselessLiteral("patch to be changed")
                                | pp.CaselessLiteral("soluble")
                                | pp.CaselessLiteral("replace")
                                | pp.CaselessLiteral("inject")
                                | pp.CaselessLiteral("subcutaneous")
                                | pp.CaselessLiteral("rinse or gargle using")
                                | pp.CaselessLiteral("for subcutaneous injection")
                                | pp.CaselessLiteral("by subcutaneous injection")
                                | pp.CaselessLiteral("intramuscular injection")
                                | pp.CaselessLiteral("for deep intramuscular injection")
                                | (pp.CaselessLiteral("instil") + pp.Optional(pp.CaselessLiteral('l')))
                                | pp.CaselessLiteral("inhale")
                                | (pp.CaselessLiteral("apply") | pp.CaselessKeyword("applt"))
                                )

    asdirected = pp.Optional(direction_post) + \
                 (pp.CaselessLiteral("as directed")
                  | pp.CaselessLiteral("up to directed")
                  | pp.CaselessLiteral("as instructed")
                  | pp.CaselessLiteral("mud")
                  | pp.CaselessLiteral("as req")
                  | pp.CaselessLiteral("prn")
                  | pp.CaselessLiteral("mds")
                  | pp.CaselessKeyword("nn")
                  | pp.CaselessLiteral("according to requirements")
                  | pp.CaselessLiteral("follow the pack instructions")
                  | pp.CaselessLiteral("following instructions")
                  | pp.CaselessKeyword("aa")
                  | pp.CaselessLiteral("when required")
                  | pp.CaselessLiteral("when needed")
                  | pp.CaselessLiteral("if needed")
                  | pp.CaselessLiteral("as dir")
                  | pp.CaselessLiteral("as direcetd.")
                  | pp.CaselessLiteral("as dircted.")
                  | pp.CaselessLiteral("asd")
                  | pp.CaselessKeyword("ad")
                  | pp.CaselessKeyword("as")
                  | pp.CaselessLiteral("given")
                  ).setParseAction(pp.replaceWith("na"))

    na = (pp.Optional(direction_pre) + asdirected).setResultsName("na")
    fulldose1 = Parser.unitmed \
                + howmany.setResultsName("qty_per_dose") \
                + Parser.unitmed \
                + pp.Optional(direction_post) \
                + vague.setResultsName("vague") \
                + howoften \
                + timespec
    fulldose2 = Parser.unitmed \
                + direction_post \
                + vague.setResultsName("vague") \
                + howoften \
                + timespec
    tersedose1 = direction_post \
                 + howoften \
                 + timespec
    tersedose2 = direction_pre \
                 + howmany.setResultsName("qty_per_dose") \
                 + Parser.unitmed \
                 + vague.setResultsName("vague") \
                 + howoften \
                 + timespec
    tersedose2a = howmany.setResultsName("qty_per_dose") \
                  + Parser.unitmed \
                  + timespec
    tersedose2b = howmany.setResultsName("qty_per_dose") \
                  + Parser.unitmed \
                  + howoften \
                  + timespec
    tersedose2c = direction_pre \
                  + timespec
    tersedose2d = howmany.setResultsName("qty_per_dose") \
                  + timespec
    tersedose2e = howmany.setResultsName("qty_per_dose") \
                  + direction_pre \
                  + timespec
    tersedose3 = direction_pre \
                 + Parser.unitmed \
                 + vague.setResultsName("vague") \
                 + howoften \
                 + timespec
    tersedose4 = direction_pre \
                 + howmany.setResultsName("qty_per_dose") \
                 + Parser.unitmed \
                 + timespec
    tersedose4a = howmany.setResultsName("qty_per_dose") \
                  + Parser.unitmed \
                  + direction_pre \
                  + timespec
    tersedose4b = direction_pre \
                 + howmany.setResultsName("qty_per_dose") \
                 + Parser.unitmed
    tersedose5 = direction_pre \
                 + howoften \
                 + Parser.unitmed
    tersedose6 = vague.setResultsName("vague") \
                 + Parser.unitmed \
                 + howoften \
                 + timespec
    tersedose7 = howmany.setResultsName("qty_per_dose") | timespec
    tersedose8 = timespec

    # Patterns for volume doses (0.5ml)
    voldose0 = volhowmuch.setResultsName("vol") \
               + howoften \
               + timespec
    voldose0a = howmany.setResultsName("qty_per_dose") \
                + volhowmuch.setResultsName("vol") \
                + howoften \
                + timespec
    voldose0b = howmany.setResultsName("qty_per_dose") \
                + volhowmuch.setResultsName("vol") \
                + Parser.unitmed \
                + howoften \
                + timespec
    voldose0c = howmany.setResultsName("qty_per_dose") \
                + volhowmuch.setResultsName("vol") \
                + Parser.unitmed \
                + volhowmuch \
                + timespec
    voldose1 = howmany.setResultsName("qty_per_dose") \
               + volhowmuch.setResultsName("vol") \
               + Parser.unitmed \
               + direction_post \
               + timespec
    voldose1a = volhowmuch.setResultsName("vol") \
                + Parser.unitmed \
                + direction_post \
                + timespec
    voldose1b = howmany.setResultsName("qty_per_dose") \
                + volhowmuch.setResultsName("vol") \
                + Parser.unitmed \
                + direction_post \
                + howoften \
                + timespec
    voldose2 = howmany.setResultsName("qty_per_dose") \
               + volhowmuch.setResultsName("vol") \
               + Parser.unitmed \
               + timespec
    voldose3 = volhowmuch.setResultsName("vol") \
               + timespec
    voldose4 = volhowmuch.setResultsName("vol") \
               + howmany.setResultsName("qty_per_dose") \
               + timespec
    voldose5 = volhowmuch.setResultsName("vol")
    voldose = pp.Optional(direction_pre) + (
                na | voldose0.setResultsName("v0") | voldose0a.setResultsName("v0a")
                | voldose0b.setResultsName("v0b") | voldose0c.setResultsName("v0c") | voldose1.setResultsName("v1")
                | voldose1a.setResultsName("v1a") | voldose1b.setResultsName("v1b")
                | voldose2.setResultsName("v2") | voldose3.setResultsName("v3")
                | voldose4.setResultsName("v4")
                | voldose5)

    # Patterns for non-volume doses (3 tablets)
    dose = (pp.Optional(wthowmuch)
            + (na.setResultsName("na")
               | fulldose1.setResultsName("fd1") | fulldose2.setResultsName("fd2")
               | tersedose1.setResultsName("td1") | tersedose2.setResultsName("td2")
               | tersedose2a.setResultsName("td2a") | tersedose2b.setResultsName("td2b")
               | tersedose2c.setResultsName("td2c") | tersedose2d.setResultsName("td2d")
               | tersedose2e.setResultsName("td2e") | tersedose3.setResultsName("td3")
               | tersedose4.setResultsName("td4") | tersedose4a.setResultsName("td4a")
               | tersedose4b.setResultsName("td4b") | tersedose5.setResultsName("td5")
               | tersedose6.setResultsName("td6") | tersedose7.setResultsName("td7")
               | tersedose8.setResultsName("td8"))) \
           | wthowmuch.setResultsName("wt")

    # Patterns for syringes.  Total volume may be taken from NDQ and multiply out the given volume.
    # e.g. "0.6mg (0.1ml) daily by subcutaneous..."
    syrdose1 = wthowmuch.setResultsName("wt") \
               + syrhowmuch.setResultsName("syrvol") \
               + timespec \
               + direction_pre
    syrdose1b = syrhowmuch.setResultsName("syrvol") \
                + direction_pre
    # e.g. "0.1ml daily"
    syrdose2 = syrhowmuch.setResultsName("syrvol") \
               + timespec
    # e.g. "0.6mg once weekly by subcutaneous..."
    syrdose3 = wthowmuch.setResultsName("syrvol") \
               + howoften \
               + timespec
    syrdose3a = howoften + timespec
    syrdose3b = timespec
    syrdose3c = howmany + pp.Optional(Parser.unitmed) + howoften + timespec
    # e.g. "every 2 weeks subcutaneous": no volumes; assume 1 per timefreq
    syrdose4 = timespec \
               + direction_pre
    syrdose4a = howmany + timespec \
                + direction_pre
    syrdose5 = direction_pre \
               + howoften \
               + timespec
    syrdose = syrdose1 | syrdose1b | syrdose2 | syrdose3 | syrdose3a | syrdose3b | syrdose3c | syrdose4 | syrdose4a | syrdose5 | na

    def __init__(self):
        """Initialise grammar."""
        pass

    def volparse(self, exp):
        """Parse dose definitions where quantity unit is ml."""
        try:
            results = self.voldose.parseString(exp)
            if isinstance(results, str):
                print(results, file=sys.stderr)
            else:
                print(results.dump, file=sys.stderr)
            return results, True
        except:
            return exp, False

    def parse(self, exp):
        """Parse all non-ml dose definitions."""
        try:
            results = self.dose.parseString(exp)
            return results, True
        except:
            return exp, False

    def syringe_parse(self, exp):
        """Parse dose definitions where quantity unit is "pre-filled disposable injection"."""
        try:
            results = self.syrdose.parseString(exp)
            return results, True
        except:
            return exp, False


if __name__ == "__main__":
    ### Parse the arguments
    parser = argparse.ArgumentParser(description="Parse polypharmacy data.")
    parser.add_argument('-f', '--csvfile', dest="csvfile", default=None,
                        help="CSV file to parse.")
    parser.add_argument('-l', '--limit', dest="limit", default=None,
                        help="Maximum number of records to parse.")
    parser.add_argument('-o', dest="failcsv", default=None,
                        help="Output CSV for records which failed to parse")
    parser.add_argument('-e', dest="errcsv", default=None,
                        help="Output CSV for records which generated exceptions")
    parser.add_argument('-s', dest="offset", default=None,
                        help="Offset for file (default 1, to ignore SITE field)")
    parser.add_argument('-x', '--debug', dest="debug", action="store_true", default=False)

    args = parser.parse_args()
    debug = args.debug

    f_csv = args.csvfile
    limit = None
    if args.limit:
        limit = int(args.limit)

    # For CSV output to stdout
    stdwriter = csv.writer(sys.stderr, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    F_OFFSET = 1  # offset to cope with initial `site' field
    if args.offset:
        F_OFFSET = int(args.offset)

    DATE_ISS = F_OFFSET + 3  # Fields in the CSV
    NDQ = F_OFFSET + 4  # Name, Dosage and Quantity
    DOSE = F_OFFSET + 5
    QTY = F_OFFSET + 6
    QTY_UNIT = F_OFFSET + 7

    ### Read in the CSV
    recs = []
    with open(f_csv, newline='', mode='r') as fid:
        reader = csv.reader(fid, delimiter=',', quotechar='"')
        hdr = next(reader)
        hdr = hdr + ["Dose (orig)"]  # Remember pre-prelacment (for debug)
        ii = 1
        try:
            for line in reader:
                ii += 1
                recs.append(line + [line[DOSE]])
        except:
            print("ERROR READING CSV (last line):", ii, file=sys.stderr)
            stdwriter.writerow(line)

    print("total: ", len(recs), file=sys.stderr)
    print(file=sys.stderr)
    for fieldid in [F_OFFSET + a for a in [0, 4, 5, 6, 7]]:
        print(hdr[fieldid], end=': ', file=sys.stderr)
        print(len(set([rec[fieldid] for rec in recs])), file=sys.stderr)

    print(file=sys.stderr)

    prescrs = [rec[:DOSE] + [str(rec[DOSE]).lower()] + rec[DOSE + 1:] for rec in recs]
    print(hdr[DOSE], end=' (lcase) : ', file=sys.stderr)
    print(len(set([rec[DOSE] for rec in prescrs])), file=sys.stderr)

    # rec[DOSE].replace(" mg", "mg").replace(" ml", "ml")
    # Replace some basic typos.
    prescrs = [rec[:DOSE] + [
                  re.sub(" mg", "mg",
                         re.sub(" ml", "ml",
                                re.sub("mls", "ml",
                                    re.sub(r"([0-9])(daily)", r"\1 \2",
                                           re.sub(r"([^0-9. ])([0-9])", r"\1 \2",
                                                  re.sub(r"(^[0-9. ]+)", r"\1 ",
                                                         re.sub("[\s]+", ' ', rec[DOSE])
                                                         )
                                                  )
                                              )
                                       )
                                )
                         )
                ] + rec[DOSE + 1:] for rec in prescrs]
    print(hdr[DOSE], end=' (spaces) : ', file=sys.stderr)
    print(len(set([rec[DOSE] for rec in prescrs])), file=sys.stderr)

    ### Pre-parsing: Replacements and exclusions.
    # Replacement text before parsing (TBC make this more generic with REs)
    # Typos: could match with distance.
    replacements_dose = [["dissolve the contents of", ''],
                         ["dissolve contents of", ''],
                         ["to affected areas of the scalp", ''],
                         ["into both eyes", ''],
                         ["in each nostril", ''],
                         ["in water and", ''],
                         ["dilution of linctus, ", ''],
                         ["subcutneous", "subcutaneous"],
                         ["teaspoonfful", "teaspoonful"],
                         ["teaspoonfull", "teaspoonful"],
                         ["teaspoonsful", "teaspoonfuls"],
                         ["spoonsful", "spoonfuls"],
                         ["di[s]+olve", "dissolve"],
                         ["befpre", "before"],
                         ["weelkly", "weekly"],
                         ["0 ne", "One"],
                         ["at [0-9][ap]m", ''],
                         ["in the nebuliser", ''],
                         ["^caplets,", ''],
                         ["^nexium:", ''],
                         ["in nomad tray", ''],
                         ["rescue pack", ''],
                         ["hlaf", "half"],
                         ["halh", "half"],
                         ["at the same time of day", ''],
                         ["under tongue", ''],
                         ["and then close mouth", ''],
                         ["into the affected eye(s)", ''],
                         ["by the hospital", ''],
                         ["to protect stomach lining from steroids", ''],
                         ["apply sparingly", ''],
                         ["for use", ''],
                         ["into the affected ear", ''],
                         ["with insulin pen", ''],
                         ["the contents of", ''],
                         ["after 12 weeks of colevet then", ''],
                         ["mouth girgfle", ''],
                         ["on the same day", ''],
                         ["rt eye", ''],
                         ["at 2pm", ''],
                         ["bd/prn", "bd"],
                         ["notes for patient.*$", ''],
                         ["notes for dispenser.*$", ''],
                         ["^otc", ''],
                         ["after food", ''],
                         ["at teatime", ''],
                         ["can manage", ''],
                         ["into the [a-z][a-z]*", ''],
                         ["\([0-9]+ml\)", ''],
                         [',', ''],
                         ['1 oml', '10 ml'],
                         ['^-', ''],
                         ['\(', ''],
                         ['\)', ''],
                         ["\+", ' '],
                         ["[ ]*-[ ]*", '-'],
                         ["(right|left|r|l) (arm|leg|knee|eye|wrist|elbow)", '']  # some specifics for maypole
                         ]

    # Exclusions: records like this cannot be parsed (may also duplicate the grammar).
    # lowercase - note these are parsed as words.
    # Format : [excl_text, [except_qty_unit]] where 2nd field is optional list of qty_units to which excl does not apply
    exclusions_dose = [["as dir", []],
                       ["as per", []],
                       ["as directed", []],
                       ["at directed", []],
                       ["up to directed", []],
                       ["as instructed", []],
                       ["as req", []],
                       ["as required", []],
                       ["apply od", []],
                       ["according to requirements", []],
                       ["follow the pack instructions", []],
                       ["following instructions", []],
                       ["when required", []],
                       ["when needed", []],
                       ["if needed", []],
                       ["as direcetd", []],
                       ["as dircted", []],
                       ["apply thinly", []],
                       ["shampoo hair", []],
                       ["all over", []],
                       ["one applicatorful", []],
                       ["^immediately$", []],
                       ["for information only", []],
                       ["^suppl.* in[ -]house$", []],
                       ["do not dispense", []],
                       ["^to fit inhaler$", []],
                       ["sls", []],
                       ["please follow.*instruction", []],
                       ["td 4277", []],
                       ["waf h33", []],
                       ["bn-1334823", []],
                       ["uk product", []],
                       ["^pre procedure$", []],
                       ["^for intramuscular injection$", []],
                       ["drop[s]", ["dose", "unit dose"]],
                       ["spray[s]", ["dose", "unit dose"]]
                       ]

    exclusions_ndq = ["emollient",
                      "gel",
                      "shampoo",
                      "bath additive",
                      "toothpaste",
                      "scalp application",
                      "nail lacquer",
                      "balm",
                      "lotion",
                      "gel"]

    exclusions_qtyu = ["device",
                       "gram",
                       "inhaler",
                       "ampoule",
                       "cartridge",
                       "pack"]

    # Multipliers for the final "days" count
    multiply_dose = [["^.*into both eyes.*$", 2],
                     ["^.*in each nostril.*$", 2]
                     ]

    grNDQ = ParserNDQ()
    grDose = ParserDose()
    ii = 0

    ### Reporting: Open CSVs for failures to parse, and unspecified errors
    failwriter = None
    if args.failcsv:
        fidf = open(args.failcsv, 'w', newline='')
        failwriter = csv.writer(fidf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        failwriter.writerow(hdr + ["reason"])
    else:
        fidf = None
    if args.errcsv:
        fide = open(args.errcsv, 'w', newline='')
        errwriter = csv.writer(fide, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        errwriter.writerow(hdr + ["error"])
    else:
        fide = None

    # Parsed CSVs will be written to stdout
    outwriter = csv.writer(sys.stdout, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    outwriter.writerow(hdr + ["Vol", "Qty/Dose", "Frequency", "Dose/Day", "Days", "End Date"])

    ### Go through the prescription records one by one
    for prescr in prescrs:
        if limit and ii > limit: break

        # Initialise / reset values.
        failreason = errreason = ""
        bEdited = False  # True if pre-parsing changes to DOSE
        bParsed = True  # False if the grammar parse fails
        bError = False  # True if we catch an error that we can't handle
        multiplier = 1  # Set from multiply_dose

        print(prescr[DOSE], end=' ', file=sys.stderr)

        # Check we can parse the Quantity field (some are blank).
        try:
            total_qty = float(prescr[QTY])
        except Exception as e:
            bError = True
            errreason = "Quantity field not understood: " + str(e)

        if not bError:
            # Check for any multipliers
            for text, mult in multiply_dose:
                if re.match(text, prescr[DOSE]):
                    multiplier *= mult

            # Remove superfluous text -- may reduce accuracy
            for repl in replacements_dose:
                newprescr = prescr[:DOSE] + [re.sub(repl[0], repl[1], prescr[DOSE]).strip()] + prescr[DOSE + 1:]
                if newprescr != prescr:
                    prescr = newprescr
                    bEdited = True

            # There are some we cannot parse (including any which are now empty)
            if bEdited:
                print("-->", prescr[DOSE], end=' ', file=sys.stderr)

            # Don't parse if any of the excluded phrases (as words) in Dose, NDQ or Quantity Unit -- or if now empty
            # Report the first match that caused the exclusion.
            gg = [re.search("\\b" + e[0] + "\\b", prescr[DOSE]) for e in exclusions_dose if prescr[QTY_UNIT] not in e[1]]
            if any(gg):
                print("'[na]'", file=sys.stderr)
                failreason = "excluded text in DOSE (" \
                             + [re.sub('[\^\.\*\$]', '', re.sub('\\\\b', '', mm.re.pattern)) for mm in gg if mm is not None][0] + ')'
                bParsed = False
            else:
                gg = [re.search("\\b" + e + "\\b", prescr[NDQ]) for e in exclusions_ndq]
                if any(gg):
                    print("'[na]'", file=sys.stderr)
                    failreason = "excluded text in NDQ (" \
                                 + [re.sub('[\^\.\*\$]', '', re.sub('\\\\b', '', mm.re.pattern)) for mm in gg if mm is not None][0] + ')'
                    bParsed = False
                else:
                    gg = [re.search("\\b" + e + "\\b", prescr[QTY_UNIT]) for e in exclusions_qtyu]
                    if any(gg):
                        print("'[na]'", file=sys.stderr)
                        failreason = "excluded Quantity Unit (" \
                                     + [re.sub('[\^\.\*\$]', '', re.sub('\\\\b', '', mm.re.pattern)) for mm in gg if mm is not None][0] + ')'
                        bParsed = False

            if bParsed:
                if not prescr[DOSE]:
                    print("'[na]'", file=sys.stderr)
                    failreason = "DOSE empty after modification"
                    bParsed = False
                else:
                    # Parse the remaining
                    # 1. Parse the Name-Dose-Quqntity field looking for syringe volumes
                    syr_totalvol = ndq_howmuch = 1
                    bSuccessNDQ = bSuccess = False

                    # First parse the Name, Dose and Quantity field (NDQ) - maybe not only here
                    if prescr[QTY_UNIT] == DISP_INJ:
                        ppNDQ, bSuccessNDQ = grNDQ.ndqparse(prescr[NDQ])
                        if debug:
                            if isinstance(ppNDQ, str):
                                print(ppNDQ, file=sys.stderr)
                            else:
                                print(ppNDQ.dump, file=sys.stderr)

                        pp, bSuccess = grDose.syringe_parse(prescr[DOSE])
                        if debug:
                            if isinstance(pp, str):
                                print(pp, file=sys.stderr)
                            else:
                                print(pp.dump, file=sys.stderr)

                        if bSuccessNDQ:
                            if ppNDQ.howmuch:
                                ndq_howmuch = float(''.join(ppNDQ.howmuch))

                            if ppNDQ.totalvol:
                                syr_totalvol = float(''.join(ppNDQ.totalvol))
                            else:
                                if not isinstance(pp, str) and pp.syrvol:
                                    syr_totalvol = float(''.join(pp.syrvol))
                        else:
                            if bSuccess and pp.syrvol:
                                syr_totalvol = float(''.join(pp.syrvol))  # ???

                    # For ml, parse looking for volume information
                    elif prescr[QTY_UNIT] == "ml":
                        pp, bSuccess = grDose.volparse(prescr[DOSE])
                        if debug:
                            if isinstance(pp, str):
                                print(pp, file=sys.stderr)
                            else:
                                print(pp.dump, file=sys.stderr)

                    # Else parse looking for standard dose information (e.g. # tablets # often)
                    else:
                        pp, bSuccess = grDose.parse(prescr[DOSE])
                        if debug:
                            if isinstance(pp, str):
                                print(pp, file=sys.stderr)
                            else:
                                print(pp.dump, file=sys.stderr)

                    print("==>", pp, end='', file=sys.stderr)
                    if not bSuccess:
                        bParsed = False
                        failreason = "failed parsing by DOSE grammar "
                        if prescr[QTY_UNIT] == "ml":
                            failreason += "(ml)"
                        else:
                            failreason += "(not ml)"
                        print(" FAILED", file=sys.stderr)
                    else:
                        print(file=sys.stderr)
                        if pp.na:
                            bParsed = False
                            print(" NA", file=sys.stderr)
                            failreason = "grammar returned As Directed"

        # Interpret parse results
        if bParsed and not bError:
            # Assume defaults of 1
            if not pp.dose_per_day:  # Should never happen?
                dose_per_day = 1
            else:
                # e.g. weekly or monthly
                dose_per_day = parse_poss_frac(pp.dose_per_day)

            if pp.vol:
                if isinstance(pp.vol, str):
                    vol = float(pp.vol)
                else:
                    vol = float(''.join(pp.vol))
            else:
                vol = 1

            if not pp.qty_per_dose:  # How mnay tablets etc. per dose
                qty_per_dose = 1
            else:
                qty_per_dose = float(pp.qty_per_dose)

            if not pp.freq:  # How many time units, e.g. n days or m months
                freq = 1
            else:
                freq = parse_poss_frac(pp.freq)

            # Scale totalvol (default 1) if we are dealing with a syringe-type delivery where we
            # know the amount it delivers per unit.
            if pp.syrvol:
                syr_units = syr_totalvol / float(''.join(pp.syrvol))
            else:
                syr_units = 1

            # Calculations based on parsed results
            try:
                doses = total_qty / qty_per_dose / vol * syr_units
                days = doses / dose_per_day / freq / multiplier
            except Exception as e:
                errreason = "ERROR caught calculating doses and days: " + str(e)
                bError = True

            if not bError:
                # Work out start date as Unix seconds since epoch (00:00:00 01/01/1970)
                start_date = end_date = 0
                try:
                    start_date = int(time.mktime(time.strptime(prescr[DATE_ISS], "%d-%b-%y")))
                except ValueError as ve:
                    # Sometimes Excel has not converted the date, so try it as integer days > 01/01/1900.
                    try:
                        start_date = int(prescr[DATE_ISS])
                    except Exception as e:
                        bError = True
                        errreason = "Error calculating start date: " + str(e) + ";" + str(ve)

                    base = int(time.mktime(time.strptime("31-12-1900", "%d-%m-%Y")))
                    start_date = base + start_date * 24 * 60 * 60

                # Work out end date
                try:
                    end_date = start_date + (days * 24 * 60 * 60)
                    end_date = time.strftime("%d-%b-%Y", time.localtime(end_date))
                except Exception as e:
                    bError = True
                    errreason = "Error calculating end date: " + str(e)
            else:
                total_qty = vol = qty_per_dose = freq = dose_per_day = days = end_date = "na"

        # Output CSV records appropriately
        if bError:
            if fide:
                errwriter.writerow(prescr + [errreason])
            else:
                stdwriter.writerow(prescr + [errreason])
        elif not bParsed:
            if fidf:
                failwriter.writerow(prescr + [failreason])
            else:
                stdwriter.writerow(prescr + [failreason])
        else:
            outwriter.writerow(prescr + [vol, qty_per_dose, freq, dose_per_day, days, end_date])
            print(
                "QTY:", total_qty, "at", vol, "ml", qty_per_dose, "per dose, FREQ:", dose_per_day, "per", freq,
                "days (multiplier", multiplier, ") is", days, "days (totalvol", syr_totalvol, ')', file=sys.stderr)

        ii += 1

    if args.failcsv:
        fidf.close()
