#!/usr/bin/env python
"""PPTA mining algorithm for MiMMS polypharmacy prescription data.

Phil Weber 2017-08-17
2017-10-06 Add option to output drugs combinations.

To Do
1.  Use the ALERGIA algorithm (or similar) to learn and generalise PPTA to PDFA
    more rigorously; apply to per-patient and full PPTA.
2.  Apply generalisation to the larger model.
3.  Process-mining type generalisation / visualisation (e.g. sliders).
4.  Bottom-up and top-down clustering to identify common patterns of prescription.
5.  Interactive zoom - won't allow zooming back in due to not copying PDFA
    for efficiency.
6   Check correctness of the logic for removing empty states correct?

Note:
o  remove_empty and remove_dups only work for "Sequence PPTA"
o  add option to show duplicate prescription as new state
o  decide how to deal with end date of trace

Outline processing
- Preprocess events and read into (transition-emitting) "Sequence PPTA" for a single patient (uses PDFA class).
  - Set prescription start and end events in time order.
  - Address any data issues.
- Output stats from this.
- Convert to (state-emitting) S-PPTA (S-PDFA class).
- (Output duplicate stats).
- Visualise with Graphviz dot.
- Naively convert to S-PDFA and visusalise.

Notes
o  Updating 0-day to 1-day prescriptions.
"""

import sys
from os import system, path
import argparse
import csv
import time
import operator
import bisect
import copy
import itertools
import pickle  # cPickle not available at SoCS
import numpy as np
import curses
from py2neo import Graph

B_ZERO_TO_1 = True  # If set, modify zero-day prescriptions to 1 day.  (Can be generalised later).

# input field IDs as output by prep_data.py
ANON_ID = 0
DATE_ISSUE = 1
MED = 2
END_DATE = 3
patt_date_issue = "%d-%b-%y"  # e.g. 01-Jan-17
patt_end_date = "%d-%b-%Y"  # e.g. 01-Jan-2017
secs_day = 60 * 60 * 24
secs_year = secs_day * 365

LAST_DATE = "31-DEC-16"  # Assumed end date of prescription period (for final transition/state)

e_event_date = 0  # indices to event list
e_se = 1
e_med = 2
e_pair_date = 3

start_tag = 'a'  # ensure start before end
end_tag = 'b'

NEO_PASS = "PASSWORD"  # Some kind of security needed

DEBUG = False
NOPRINT = False
BASETIME = 0


def getch(ii, max_ii):
    input()
    return ii+1

def __getch(ii, max_ii):
    """Get single character.

    :param ii: index to be modified to control visualisation.
    :param max_ii: do not exceed.

    This seems surprisingly difficult in Python.
    This is not the best approach since it spins until character received.
    (And we don't particularly want a curses screen.)
    """
    def _getch(stdscr):
        # do not wait for input when calling getch
        stdscr.nodelay(1)
        
        while True:
            # get keyboard input, returns -1 if none available
            c = stdscr.getch()
            if c != -1:
                c = chr(c)
                if c == 'j':
                    stdscr.addstr("Down")
                    return(c)
                if c == 'k':
                    stdscr.addstr("Up")
                    return(c)
                stdscr.refresh()

    xx = curses.wrapper(_getch)
    if xx == "j":  # basic control
        ii = ii+1 if ii < max_ii else ii
    elif xx == "k":
        ii = ii-1 if ii > 0 else 0
    # print(ii)
    # input()
    return(ii)


class PDFA(object):
    globii = 0

    """Transition-emitting Probabilistic Deterministic Finite Automaton.

    Initially only used as PPTA.
    """
    def __init__(self, pat_id=None, n_raw_events=0):
        """Initialise PDFA with single start state.

        :param pat_id: patient ID for which this PDFA is initialised: NB: as a "Sequence PPTA" for one patient.
        :param n_raw_events: no. of prescription events for this patient in the original data
        """
        self.q0 = self.State('q' + str(next(gen_id)), 1, pat_id)  # single initial state ...
        self.qF = [self.q0]  # ... is also the single final state (but allow for multiple)
        self.states = [self.q0]  # states in this automaton
        self.transitions = []  # transitions in this automaton

        self.patients = {pat_id : n_raw_events}  # Patients annotated on this PDFA

    def add_pdfa_state(self, count=0, q_id=None, pat_id=None):
        """Add a state to this PDFA.

        :params: see State.__init__
        """
        if not q_id:
            q_id = 'q' + str(next(gen_id))
        qq = self.State(q_id, count, pat_id)
        self.states.append(qq)
        return qq

    def add_pdfa_trans(self, q_fr, q_to, meds, pat_id, start=0, end=None):
        """Add a PDFA transition between two states, copying the supplied list of medications.

        :params: see Trans.__init__
        """
        tt = self.Trans(q_fr, q_to, meds.copy(), 1, pat_id, start, end)
        self.transitions.append(tt)

        # Adjust end states
        if q_fr in self.qF:
            self.qF.remove(q_fr)
            self.qF.append(q_to)

        if DEBUG:
            print("Added trans: ", end='')
            print(self.transitions[-1].meds)
        return tt

    def delete_trans_state(self, tt):
        """Delete transition tt and its target state if empty.

        :param: tt: transition to delete.
        """
        q_fr = tt.q_fr
        q_to = tt.q_to
        q_fr.t_out.remove(tt)
        q_to.t_in.remove(tt)
        if len(q_to.t_in) <= 0:
            self.states.remove(q_to)
        self.transitions.remove(tt)

        # Adjust end states
        if q_to in self.qF:
            self.qF.remove(q_to)
            self.qF.append(q_fr)

    def add_pdfa_trans_state(self, q_fr, t_curr, meds, pat_id, start, output_log=False):
        """Add a transition from the given state, and a target state.

        :param q_fr: start state of this transition
        :param t_curr: add the end time for the previous transition
        :param meds:
        :param pat_id:
        :param start: start time of this transition
        :param output_log: also output/amend consolidated event log record
        """
        q_to = self.add_pdfa_state(1, pat_id=pat_id)
        if t_curr:  # complete the previous transition
            t_curr.set_end(pat_id, start)
            if output_log:
                print(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(start)))  # end time
        t_new = self.add_pdfa_trans(q_fr, q_to, meds, pat_id, start)
        if output_log:
            print(pat_id, end=',')  # case ID
            print(flat_meds(meds, pp=True), end=',')  # activity ID
            print(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(start)), end=',')  # start time

        # Adjust the list of final states
        if q_fr in self.qF:
            self.qF.remove(q_fr)
        self.qF.append(q_to)

        return q_to, t_new

    def remove_empty_SeqPPTA(self, rm_no_meds=False):
        """Remove transitions (and following state) of zero durations (in "Sequence PPTA").

        Only if preceding and succeeding transition have the same label?  XXX

        Limitation: assumes sequence PPTA => 1-out and 1-in arc.

        :param rm_no_meds: remove nodes where no medication was taken.
        """
        q_fr = self.q0
        while len(q_fr.t_out) > 0:
            if len(q_fr.t_out) > 1:
                raise Exception("Only able to remove empty transitions from 'Sequence PPTA'")

            tt = q_fr.t_out[0]  # PPTA => only 1
            q_to = tt.q_to
            # Might want 1d or 1 (prescription period) - although the end dates
            # should already include this.

            pat1 = list(tt.pat_ids.keys())[0]
            if len(tt.pat_ids.keys()) > 1 \
                or len(tt.pat_ids[pat1]["durations"]) > 1:
                raise Exception("Only able to remove empty transitions for single-patient"
                                + "'Sequence PPTA' with single duration on arc")

            start, end = tt.pat_ids[pat1]["durations"][0]
            if end - start <= 0 or (rm_no_meds and tt.meds == set()):
                # Reconnect arcs
                if len(q_to.t_out) > 0:  # Not the last transition
                    tt_new = q_to.t_out[0]
                    q_fr.t_out[0] = tt_new
                    tt_new.q_fr = q_fr
                else:
                    q_fr.t_out = q_fr.t_out[:-1]
                self.states.remove(q_to)
                self.transitions.remove(tt)

                if q_to in self.qF:  # Adjust end states
                    self.qF.remove(q_to)
                    self.qF.append(q_fr)
            else:
                q_fr = q_to

    def remove_dups_SeqPPTA(self):
        """Consolidate sequences of transitions on "Sequence PPTA" labelled with the same meds.

        Limitation: assumes PPTA = > 1 - out and 1 - in arc.
        """
        q_fr = self.q0
        while len(q_fr.t_out) > 0 \
                and len(q_fr.t_out[0].q_to.t_out) > 0:

            if len(q_fr.t_out) > 1:
                raise Exception("Only able to remove empty transitions from 'Sequence PPTA'")

            t1 = q_fr.t_out[0]  # PPTA => only 1
            q_to = t1.q_to
            t2 = q_to.t_out[0]
            t1_meds = sorted(flat_meds(t1.meds))
            t2_meds = sorted(flat_meds(t2.meds))

            if t1_meds == t2_meds:
                # Remove the first transition, updating the start date on the second
                t2.pat_ids = t1.pat_ids
                q_fr.t_out[0] = t2
                self.states.remove(q_to)
                self.transitions.remove(t1)

                if q_to in self.qF:  # Adjust end states
                    self.qF.remove(q_to)
                    self.qF.append(q_fr)
                # Do not update q_fr: recheck with new (t1, t2) pair
            else:
                q_fr = q_to

    def print(self, ptype=None, pat_id=None, n_patient=0, dotfile=None, durtype="mean", fidlog=None):
        """Simple print or dot output for ***sequence*** PPTA.

        Output plain, or to supplied dotfile and convert to EPS, or just output to stdout.
        :param ptype: "dot", "eventlog" or other
        :param:pat_id: (for title)  XXX probably not wanted
        :param dotfile: name of DOT file (.dot) to write, used to generate EPS file name (.eps)
        :param durtype: output as "mean" duration or "total" duration
        """
        if ptype == "dot":
            if dotfile:
                dotfile.replace(".dot", "")  # Strip off suffix - we will add it
                fid = open(dotfile + ".dot", 'w')
            else:
                fid = sys.stdout
            print('digraph "' + pat_id + '" {', file=fid)
            print('  graph [rankdir=TB, center=1, ranksep=".2", nodesep=".1"];', file=fid)
            print('  node [shape=box, fontsize=10];', file=fid)

        elif ptype == "eventlog" and n_patient == 1 and fidlog:
            print("Patient ID (Case ID), Meds (Activity ID), Start, End", file=fidlog)

        else:
            fid = sys.stdout

        if not pat_id:
            pat_id = list(self.patients.keys())[0]

        ii = 0
        for qq in self.states:
            if qq == self.q0:
                color = "blue"
            elif qq in self.qF:
                color = "green"
            else:
                color = "black"

            if ptype == "dot":
                node = qq.id
                ii += 1

                durations = flat_durations(qq.pat_ids)
                out_dur = sum([end - start for (start, end) in durations])
                if durtype == "mean":
                    out_dur /= len(durations)

                xlabel = "{:d}d".format(int(out_dur / secs_day))

                print('  ', node, '[label="' + node + '", xlabel="' + xlabel + 
                      '", color=' + color + ']', file=fid)

        for tt in self.transitions:
            durations = flat_durations(tt.pat_ids)
            if ptype == "dot":
                label = "\n".join(sorted(flat_meds(tt.meds)))
                if label == "":
                    label = "(none)"
                    style = ',style="dotted"'
                else:
                    style = ''

                fr_node = tt.q_fr.id
                to_node = tt.q_to.id

                # Extract flat list of durations out of the nested dictionaries
                out_dur = sum([end - start for (start, end) in durations])
                if durtype == "mean":
                    out_dur /= len(durations)

                xlabel = "{:d}d".format(int(out_dur / secs_day))
                print('  ', fr_node, "->", to_node, '[label="' + label + '",xlabel="' + xlabel + '"' + style + '];', file=fid)
            elif ptype == "eventlog" and fidlog:
                start = min([start for (start, _) in durations])
                end = max([end for (_, end) in durations])
                print(pat_id, end=',', file=fidlog)  # case ID
                print(flat_meds(tt.meds, pp=True), end=',', file=fidlog)  # activity ID
                print(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(start)), end=',', file=fidlog)
                print(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(end)), file=fidlog)

            else:
                print('--> ', end='')
                tt.print()

        if ptype == "dot":
            print('}', file=fid)

            if dotfile:
                fid.close()
                pdffile = dotfile + ".eps"
                cmd = "dot -Teps -o " + pdffile + ' ' + dotfile + ".dot"
                system(cmd)
        return

    def print_stats(self):
        """Output polypharmacy stats.

        patient ID, #prescs, #severe_interacts, #interacts, duration_stats, {rpt. for unique combinations)
        Initially, assuming per-patient sequence PPTA.
        """
        # Initialise collectors - dictionary keyed on patient ID
        stats = {}

        # Parse PPTA from root, accumulating stats
        st_q = []  # Stack of states processed

        # Push the first state
        st_q.append(self.q0)

        # Explore the stack until it is empty
        while len(st_q) > 0:
            qq = st_q.pop()  # Pop a state

            # Process the outgoing arcs and Add the next set of states
            for tt in qq.t_out:
                st_q.append(tt.q_to)  # Add the destination PPTA states
                tmeds = tuple(sorted(flat_meds(tt.meds)))
                pat_id = list(tt.pat_ids)[0]

                if pat_id not in stats:
                    stats[pat_id] = {}
                    stats[pat_id]["n_events"] = self.patients[pat_id]  # raw no. prescription events
                    stats[pat_id]["n_prescr"] = 0  # episodes = path len = set at leaf
                    stats[pat_id]["uq_meds"] = []  # Unique medications prescribed
                    stats[pat_id]["uq_prescrs"] = []  # Unique prescription combinations
                    stats[pat_id]["n_ints"] = 0  # We will probably want to store the actual ints?
                    stats[pat_id]["n_st_ints"] = 0
                    stats[pat_id]["uq_ints"] = []  # Unique interactions
                    stats[pat_id]["uq_st_ints"] = []  # Unique interactions
                    stats[pat_id]["n_durs"] = 0
                    stats[pat_id]["min_dur"] = secs_year * 999
                    stats[pat_id]["tot_dur"] = 0
                    stats[pat_id]["max_dur"] = 0

                if len(tmeds) > 0:
                    stats[pat_id]["n_prescr"] += 1  # sum of steps on path
                    stats[pat_id]["n_ints"] += len(tt.ints)
                    stats[pat_id]["n_st_ints"] += len(tt.st_ints)

                    # Extract flat list of durations out of the dictionary
                    durations = tt.pat_ids[pat_id]["durations"]
                    stats[pat_id]["n_durs"] += len(durations)
                    stats[pat_id]["tot_dur"] += sum([end - start for (start, end) in durations])
                    min_dur = min([end - start for (start, end) in durations])
                    if min_dur < stats[pat_id]["min_dur"]:
                        stats[pat_id]["min_dur"] = min_dur
                    max_dur = max([end - start for (start, end) in durations])
                    if max_dur > stats[pat_id]["max_dur"]:
                        stats[pat_id]["max_dur"] = max_dur

                    for med in tmeds:
                        if med not in stats[pat_id]["uq_meds"]:
                            stats[pat_id]["uq_meds"].append(med)
                    if set(tmeds) not in stats[pat_id]["uq_prescrs"]:
                        stats[pat_id]["uq_prescrs"].append(set(tmeds))
                    for interact in tt.ints:
                        if interact not in stats[pat_id]["uq_ints"]:
                            stats[pat_id]["uq_ints"].append(interact)
                    for st_int in tt.st_ints:
                        if st_int not in stats[pat_id]["uq_st_ints"]:
                            stats[pat_id]["uq_st_ints"].append(st_int)

        for pat_id in stats:
            print(pat_id +
                  ',' + str(stats[pat_id]["n_events"]) +
                  ',' + str(stats[pat_id]["n_prescr"]) +
                  ',' + str(len(stats[pat_id]["uq_meds"])) +
                  ',' + str(len(stats[pat_id]["uq_prescrs"])) +
                  ',' + str(stats[pat_id]["n_ints"]) +
                  ',' + str(stats[pat_id]["n_st_ints"]) +
                  ',' + str(len(stats[pat_id]["uq_ints"])) +
                  ',' + str(len(stats[pat_id]["uq_st_ints"])) +
                  ',' + "{:0.2f}".format(stats[pat_id]["min_dur"] / secs_day) +
                  ',' + "{:0.2f}".format(stats[pat_id]["tot_dur"] / stats[pat_id]["n_durs"] / secs_day) +
                  ',' + "{:0.2f}".format(stats[pat_id]["max_dur"] / secs_day)
                  )
        return

    def merge(self, seq_ppta):
        """Merge per-patient sequence PPTA into this one.

        :param seq_ppta: sequence "PPTA" to merge in.
        """
        self.patients.update(seq_ppta.patients)

        q, q_ = self.q0, seq_ppta.q0

        # Track when following the models in parallel
        b_diverged = False
        while len(q_.t_out) > 0:
            t_seq = q_.t_out[0]  # Transition in the merging PPTA
            curr_meds = set(flat_meds(t_seq.meds))  # (Only one out_arc)

            q_ = t_seq.q_to  # Move in seq_ppta
            if not b_diverged:
                if len(q.t_out) <= 0:  # At leaf of PPTA we are merging into
                    b_diverged = True
                else:
                    if curr_meds in [set(flat_meds(t.meds)) for t in q.t_out]:
                        t_upd = [t for t in q.t_out if set(flat_meds(t.meds)) == curr_meds][0]

                        # Move in self
                        q = t_upd.q_to

                        # Update stats
                        new_pat = list(t_seq.pat_ids)[0]  # One only
                        t_upd.pat_ids[new_pat] = copy.deepcopy(t_seq.pat_ids[new_pat])

                        # No need to update meds or ints; counts are per pat_id.
                    else:
                        b_diverged = True

            if b_diverged:
                q_new = self.add_pdfa_state(count=q_.count)

                q_fr = q  # state to attach at
                q_to = q_new
                meds = t_seq.meds
                pat_id = list(t_seq.pat_ids)[0]
                t_new = self.add_pdfa_trans(q_fr, q_to, meds, pat_id)
                t_new.pat_ids = copy.deepcopy(t_seq.pat_ids)  # Copy the patient visit stats
                q = q_new  # Move in current PDFA


    # Sub-classes of the PDFA class.
    class State(object):
        """State in a transition-emitting PDFA (marks change from pharmacy status).

        (TBC: May be limited to PPTA.)
        """
        def __init__(self, q_id, count=0, pat_id=None):
            """Initialise a state in a PDFA.

            :param q_id: ID of the new state (convention: q{int}),
            :param count: How many times was this state passed in the data.
            :param pat_id: Single patient ID for which this state is initialised.
            """
            self.id = q_id
            self.count = count  # How many times was the state visited?
            self.pat_ids = set()  # Set of patient prescription traces which visited the state
            if pat_id:
                self.pat_ids.add(pat_id)  # To avoid splitting string into set of chars

            self.t_out = []  # For ease of parsing
            self.t_in = []

        def __lt__(self, qq):
            """Define state inequality based on string ID."""
            return self.id < qq.id

        def print(self):
            print(self.id, str(self.pat_ids))

    class Trans(object):
        """Transtion between transition-emitting PDFA states.

        Marks a unique pharmacy combination).
        """
        def __init__(self, q_fr, q_to, meds, count=0, pat_id=None, start=None, end=None):
            """Initialise a transition between two PDFA states.

            NB: Initialises for just one patient ID.

            :param q_fr: preceding state
            :param q_to: succeeding state
            :param meds: set of medication represented by this transition
            :param count: How many times was this transition passed.
            :param pat_id: Single patient ID.
            :param start: Start time of transition, may not be empty
            :param end: End time of transition, may initially be empty
            """
            self.id = q_fr.id + '_' + q_to.id  # May not be necessary
            self.q_fr = q_fr  # Source and destination states
            self.q_to = q_to
            self.meds = meds  # Set of medications which represents the symbol on this transition
            self.ints, self.st_ints = chk_interactions(b_DB, ints, st_ints, meds)  # Interactions found in meds
            self.pat_ids = {}  # Dict of data for patient IDs whose prescription traces
            if pat_id:
                self.pat_ids[pat_id] = {}
                self.pat_ids[pat_id]["count"] = count  # How many times was the transition passed for this patient ID?
                self.pat_ids[pat_id]["durations"] = []  # List of durations of visits for this patient ID.
                if start is None:
                    raise Exception("Cannot create PDFA trans without start time")
                self.pat_ids[pat_id]["durations"].append((start, end))
            else:
                raise Exception("Attempt to create PDFA transition without patient ID")

            q_to.t_in.append(self)
            q_fr.t_out.append(self)

        def set_end(self, pat_id, end=None):
            """Set the end time of the most recent passing of this transition by a patient ID.

            :param end: End time to update (must be time since epoch).
            """
            start = self.pat_ids[pat_id]["durations"][-1][0]
            self.pat_ids[pat_id]["durations"][-1] = (start, end)

        def print(self):
            durations = flat_durations(self.pat_ids)
            out_dur = sum([end - start for (start, end) in durations])
            duration = "{:d}d".format(int(out_dur / secs_day))
            print('[', end='')
            b_first = True
            for med in sorted(self.meds):
                if not b_first:
                    print(", ", end='')
                else:
                    b_first = False
                print(med[0], end='')
            print(']', duration)


class S_PDFA(object):
    """State-emitting Probabilistic Deterministic Finite Automaton.

    Initially used for representations and translated from (transition-emitting) PDFA.
    :param multistart: If not set, create dummy single start node if there are multiple.
                       (visualisation only).
    """
    def __init__(self, pdfa, multistart=True):
        """Initialise S-PDFA from supplied PDFA, i.e. translate transition-emitting
        to state-emitting PDFA.

        :param pdfa: transition-emitting PDFA.
        """
        if not isinstance(pdfa, PDFA):
            raise Exception("Trying to create S-PDFA not from a PDFA.")

        tt_qq = {}  # Map PDFA transitions to S-PDFA states
        self.states = []  # states in this automaton
        self.transitions = []  # transitions in this automaton
        self.patients = pdfa.patients  # patients and prescription counts

        # Define the initial and final states
        tmp_q0 = []  # If more than one, create a new q0
        self.qF = []

        # Map every transition in the PDFA onto a state in the S-PDFA.
        for tt in pdfa.transitions:
            qq = self.add_spdfa_state(next(gen_id), tt.meds, tt.ints, tt.st_ints, copy.deepcopy(tt.pat_ids))
            tt_qq[tt] = qq

            if tt.q_fr == pdfa.q0:
                tmp_q0.append(qq)
            if tt.q_to in pdfa.qF:
                self.qF.append(qq)

        # For every pair of transitions outputting a->b, add transition s_a -> s_b with # counts from t2,
        # patient list from s_a and counts from s in a->s->b.
        for tt1 in pdfa.transitions:
            for tt2 in tt1.q_to.t_out:
                # Exclude self-loops because we have the duration (not really valid but tidier visually)
                if tt_qq[tt1] != tt_qq[tt2]:
                    self.add_spdfa_trans(tt_qq[tt1], tt_qq[tt2], tt1.q_to.count, copy.deepcopy(tt_qq[tt1].pat_ids))

        # Update the initial and final states.
        for qq in self.states:
            if len(qq.t_in) <= 0 and qq not in tmp_q0:
                tmp_q0.append(qq)
            if len(qq.t_out) <= 0 and qq not in self.qF:
                self.qF.append(qq)

        # Create new dummy state with all patient IDs, no meds, no duration
        if len(tmp_q0) > 1 and not multistart:
            meds = []
            ints = []
            st_ints = []
            pat_ids = {}
            for qq in tmp_q0:
                ints += qq.ints
                st_ints += qq.st_ints
                pat_ids.update(qq.pat_ids)

            for pat_id in pat_ids:
                pat_ids[pat_id]["durations"] = [(0,0)]

            q0 = self.add_spdfa_state(next(gen_id), meds, ints, st_ints, pat_ids)
            for qq in tmp_q0:
                count = sum([tt.count for tt in qq.t_out])

                self.add_spdfa_trans(q0, qq, count, qq.pat_ids)
            self.q0 = [q0]
        else:
            self.q0 = tmp_q0


    def add_spdfa_state(self, q_id, meds, ints, st_ints, pat_ids=None):
        """Add a state to this PDFA.

        :params: see State.__init__
        """
        qq = self.State('q' + str(q_id), meds.copy(), ints, st_ints, pat_ids)
        self.states.append(qq)
        return qq

    def add_spdfa_trans(self, q_fr, q_to, count=0, pat_ids=None):
        """Add a PDFA transition between two states, copying the list of medications.

        :params: see Trans.__init__
        """
        tt = self.Trans(q_fr, q_to, count, pat_ids)
        self.transitions.append(tt)
        return tt

    def ppta_pdfa_naive(self):
        """Translate a (assumed) PPTA naively into a PDFA.

        By merging equivalent states (same set of medications).
        Note that both are state-emitting.
        """
        st_q = []  # Stack of meds (states) seen
        meds_q = {}  # map sets of meds to states retained

        for qq in self.q0:
            # Push the set of medications from the first state
            st_q.append(qq)

        # Explore the stack until it is empty
        while len(st_q) > 0:
            qq = st_q.pop()  # Pop a state
            tmeds = tuple(sorted(flat_meds(qq.meds)))

            # Add the next set of states
            for tt in qq.t_out:
                st_q.append(tt.q_to)  # Add the destination PPTA states

            # Don't merge initial "none" with others.
            if tmeds or qq not in self.q0:
                if tmeds not in meds_q.keys():
                    # Save this as the state for this set of meds
                    meds_q[tmeds] = qq
                else:
                    # Delete the state, merge the state stats, and redirect the arcs
                    exist_q = meds_q[tmeds]

                    for pat_id in qq.pat_ids:
                        if pat_id in exist_q.pat_ids:
                            # if pat_id already on the exist_q, need to merge the stats
                            exist_q.pat_ids[pat_id]["count"] += qq.pat_ids[pat_id]["count"]
                            exist_q.pat_ids[pat_id]["durations"] += qq.pat_ids[pat_id]["durations"]
                        else:
                            # else add them
                            exist_q.pat_ids[pat_id] = qq.pat_ids[pat_id]

                    self.states.remove(qq)
                    if qq in self.q0:
                        self.q0.remove(qq)
                        if exist_q not in self.q0:
                            self.q0.append(exist_q)
                    if qq in self.qF:
                        self.qF.remove(qq)
                        if exist_q not in self.qF:
                            self.qF.append(exist_q)

                    # Redirect (single, as it's a PPTA) in-arc if equivalent does not already exist
                    for t_in in qq.t_in:
                        q_fr = t_in.q_fr
                        b_found_arc = False
                        for t_out in q_fr.t_out:
                            q_to = t_out.q_to
                            if q_to == exist_q:  # Amend its stats
                                t_out.count += t_in.count

                                # Merge the stats
                                for pat_id in t_in.pat_ids:
                                    if pat_id in t_out.pat_ids:
                                        # if pat_id already on the exist_q, need to merge the stats
                                        t_out.pat_ids[pat_id]["count"] += t_in.pat_ids[pat_id]["count"]
                                        t_out.pat_ids[pat_id]["durations"] += t_in.pat_ids[pat_id]["durations"]
                                    else:
                                        # else add them
                                        t_out.pat_ids[pat_id] = t_in.pat_ids[pat_id]

                                try:
                                    self.transitions.remove(t_in)
                                    t_in.q_fr.t_out.remove(t_in)
                                    t_in.q_to.t_in.remove(t_in)
                                except ValueError:
                                    pass
                                b_found_arc = True
                        if not b_found_arc:  # Redirect the arc
                            t_in.q_to = exist_q
                            exist_q.t_in.append(t_in)

                    # Redirect out-arcs if equivalent do not already exist
                    for t_out in qq.t_out:
                        q_to = t_out.q_to
                        b_found_arc = False
                        for t_in in q_to.t_in:
                            q_fr = t_in.q_fr
                            if q_fr == exist_q:  # Amend its stats
                                t_in.count += t_out.count

                                # Merge the stats
                                for pat_id in t_out.pat_ids:
                                    if pat_id in t_in.pat_ids:
                                        # if pat_id already on the exist_q, need to merge the stats
                                        t_in.pat_ids[pat_id]["count"] += t_out.pat_ids[pat_id]["count"]
                                        t_in.pat_ids[pat_id]["durations"] += t_out.pat_ids[pat_id]["durations"]
                                    else:
                                        # else add them
                                        t_in.pat_ids[pat_id] = t_out.pat_ids[pat_id]

                                try:
                                    self.transitions.remove(t_out)
                                    t_out.q_fr.t_out.remove(t_out)
                                    t_out.q_to.t_in.remove(t_out)
                                except ValueError:
                                    pass
                                b_found_arc = True
                        if not b_found_arc:  # Redirect the arc
                            t_out.q_fr = exist_q
                            exist_q.t_out.append(t_out)
        return

    def pseudo_prob(self):
        """Convert to pseudo-probabilities by normalising the arc weights.

        :return None
        """
        for qq in self.states:
            sum_out = sum([tt.count for tt in qq.t_out])
            for tt in qq.t_out:
                tt.count /= sum_out

    def generalise(self, thresh_sm_grp=2, thresh_sm_dur_d=10, thresh_merge_subset=0.75):
        """Testing generalisation / complexity reduction methods.

        :param thresh_sm_grp: groups this size or smaller eligible for absorption
        :param thresh_sm_dur_d: nodes this duration or less eligible for absorption
        :param thresh_merge_subset: nodes overlapping by this fraction or less eligible for merge

        Initial:
        o  absorb "short-duration subset" nodes into larger group.
        o  Merge "similar group" nodes.
        """
        print("sm grp", thresh_sm_grp,
              ", sm dur", thresh_sm_dur_d,
              "d, subset", thresh_merge_subset, end='... ')

        # Sort states by number of medications in the group
        ss = sorted(self.states, key=lambda s: len(s.meds), reverse=True)

        # Look for "short-duration subset" nodes
        removed = []  # Track removed states so we don't merge into them
        print("Removing short-duration")
        for qq in ss:
            if qq not in removed:
                qq_meds = {med[0] for med in qq.meds}
                neigh = [tt.q_fr for tt in qq.t_in] + [tt.q_to for tt in qq.t_out]

                for nn in neigh:
                    nn_meds = {med[0] for med in nn.meds}

                    durations = flat_durations(nn.pat_ids)
                    duration = sum([end - start for (start, end) in durations]) / secs_day

                    if (len(nn_meds) < len(qq_meds)) \
                            and len(nn_meds) <= thresh_sm_grp \
                            and duration <= thresh_sm_dur_d:
                        removed.append(self.rm_merge(nn, qq, merge_meds=True))

        # Look for "similar group" parent-child relationship nodes (re-iterate in case states list changed).
        ss = sorted(self.states, key=lambda s: len(s.meds), reverse=True)
        print("Merging similar parent/child")
        for qq in ss:
            if qq not in removed:
                qq_meds = {med[0] for med in qq.meds}
                neigh = [tt.q_fr for tt in qq.t_in] + [tt.q_to for tt in qq.t_out]

                # Look for similar med grps
                for nn in neigh:
                    nn_meds = {med[0] for med in nn.meds}

                    similarity = len(qq_meds.intersection(nn_meds)) / float(len(qq_meds.union(nn_meds)))
                    if similarity > thresh_merge_subset:
                        removed.append(self.rm_merge(nn, qq, merge_meds=True))
                        # counter += 1

        # Look for "similar group" sibling (same parent) relationship nodes
        ss = sorted(self.states, key=lambda s: len(s.meds), reverse=True)
        print("Merging similar siblings")
        for qq in ss:
            if qq not in removed:
                # Look at its predecessors and successors
                nns = [tt.q_to for tt in qq.t_out] + [tt.q_fr for tt in qq.t_in]
                prs = list(uniq(sorted(sorted(pr) for pr in itertools.permutations(nns, 2))))

                for pr in prs:
                    if pr[0] != pr[1] and pr[0] not in removed and pr[1] not in removed:
                        q0_meds = {med[0] for med in pr[0].meds}
                        q1_meds = {med[0] for med in pr[1].meds}

                        # Look for similar med grps
                        similarity = len(q0_meds.intersection(q1_meds)) / float(len(q0_meds.union(q1_meds)))
                        if similarity > thresh_merge_subset:
                            # Arbitrarily merge into one
                            removed.append(self.rm_merge(pr[1], pr[0], merge_meds=True))
        print("done")

        return removed  # So we can track if any changes were made

    def rm_merge(self, qq_rm, qq_mg, merge_meds=False):
        """Remove node and connect its arcs to another (merging).

        XXX merge pat_ids and transitions stats

        :param qq_rm: node to remove
        :param qq_mg: node to connect to
        :param merge_meds: whether qq_rm's meds need to be merged into qq_mg's meds
        """
        if qq_rm in self.states:  # Not already moved (needs further checking?)
            print("REMOVING", qq_rm.id, "(into)", qq_mg.id, end=' ')
            self.states.remove(qq_rm)
            if qq_rm in self.qF:
                self.qF.remove(qq_rm)
                self.qF.append(qq_mg)
            if qq_rm in self.q0:
                self.q0.remove(qq_rm)
                self.q0.append(qq_mg)

            if merge_meds:
                print("and merging")
                # Merge meds accounting for usage counts (list unique meds, sum usages)
                u_meds = qq_mg.meds.union(qq_rm.meds)
                qq_mg.meds = {(x, sum([b for (a,b) in u_meds if a == x])) for (x,_) in u_meds}
                qq_mg.ints = list(set(qq_mg.ints).union(set(qq_rm.ints)))
                qq_mg.st_ints = list(set(qq_mg.st_ints).union(set(qq_rm.st_ints)))

                # Merge the state stats (could overload .update?)
                for pat_id in qq_rm.pat_ids:
                    if pat_id in qq_mg.pat_ids:
                        qq_mg.pat_ids[pat_id]["count"] += qq_rm.pat_ids[pat_id]["count"]
                        qq_mg.pat_ids[pat_id]["durations"] += qq_rm.pat_ids[pat_id]["durations"]
                    else:
                        qq_mg.pat_ids = qq_rm.pat_ids
            else:
                print()

            # Reconnect or remove arcs
            for tt_in in qq_rm.t_in:
                if not tt_in.q_fr == qq_mg \
                        and (tt_in.q_fr.id, qq_mg.id) not in \
                                [(tt.q_fr.id, tt.q_to.id) for tt in qq_mg.t_in]:
                    # (2nd part above should be impossible?)
                    tt_in.q_to = qq_mg
                    qq_mg.t_in.append(tt_in)
                else:
                    if tt_in in self.transitions:
                        self.transitions.remove(tt_in)
                    tt_in.q_fr.t_out.remove(tt_in)
                    if tt_in in tt_in.q_to.t_out:
                        tt_in.q_to.t_out.remove(tt_in)

            for tt_out in qq_rm.t_out:
                if not tt_out.q_to == qq_mg \
                        and (qq_mg.id, tt_out.q_to.id) not in \
                                [(tt.q_fr.id, tt.q_to.id) for tt in qq_mg.t_out]:
                    tt_out.q_fr = qq_mg
                    qq_mg.t_out.append(tt_out)
                else:
                    if tt_out in self.transitions:
                        self.transitions.remove(tt_out)
                    tt_out.q_to.t_in.remove(tt_out)
                    if tt_out in tt_out.q_fr.t_in:
                        tt_out.q_fr.t_in.remove(tt_out)

        return qq_rm

    def print(self, ptype=None, pat_id="", dotfile=None, durtype="mean", b_print=True):
        """Simple print or dot output for State-emitting PDFA.

        Output plain, or to supplied dotfile and convert to EPS, or just output to stdout.
        :param ptype: "dot" or other
        :param:pat_id: (for title)  XXX probably not wanted
        :param dotfile: name of DOT file (.dot) to write, used to generate EPS file name (.eps)
        :param durtype: output as "mean" duration or "total" duration
        :param b_print: boolean whether to print or simply accumulate
        """
        fid = sys.stdout
        if ptype == "dot":
            if dotfile:
                dotfile.replace(".dot", "")  # Strip off suffix - we will add it
                fid = open(dotfile + ".dot", 'w')
            print('digraph "' + pat_id + '" {', file=fid)
            print('  graph [rankdir=TB, center=1, ranksep=".2", nodesep=".1"];', file=fid)
            print('  node [shape=box, fontsize=10];', file=fid)
            print('  edge [fontsize=8];', file=fid)

        # Collect stats for thicknesses (based on count, not duration)
        counts = []
        for qq in self.states:
            counts += [qq.pat_ids[pat]["count"] for pat in qq.pat_ids]
        q_min, q_max = min(counts), max(counts)
        q_max = max(counts)
        try:
            t_min = min([tt.count for tt in self.transitions])
            t_max = max([tt.count for tt in self.transitions])
        except ValueError:  # Occurs if no transitions
            t_min, t_max = q_min, q_max

        # Output states
        bFoundInt, bFoundStInt = False, False
        for qq in self.states:
            if ptype == "dot":
                node = qq.id
                if qq in self.q0:
                    print("  st" + node, "[style=invis]", file=fid)
                    print("  st" + node, "->", node, "[arrowhead=noneo]", file=fid)
                color = ",color=black"
                if len(qq.ints) > 0:
                    color = ",color=orange"
                    bFoundInt = True
                if len(qq.st_ints) > 0:
                    color = ",color=red"
                    bFoundStInt = True
                if qq in self.qF:
                    color = ",peripheries=2,color=green"
                label = ''
                meds = sorted(flat_meds(qq.meds))  # HTML format output to show clashes
                for ii in range(len(qq.meds)):
                    med = meds[ii]
                    if ii > 0:
                        br = "<br/>"
                    else:
                        br = ''
                    if med in [a[i] for a in qq.ints for i in [0,1]] \
                    and med not in [a[i] for a in qq.st_ints for i in [0,1]]:
                        startfont = '<font color="orange">'
                        endfont = "</font>"
                    elif med in [a[i] for a in qq.ints for i in [0,1]] \
                    and med in [a[i] for a in qq.st_ints for i in [0,1]]:
                        startfont = '<font color="darkorange3">'
                        endfont = "</font>"
                    elif med not in [a[i] for a in qq.ints for i in [0,1]] \
                    and med in [a[i] for a in qq.st_ints for i in [0,1]]:
                        startfont = '<font color="red">'
                        endfont = "</font>"
                    else:
                        startfont = ''
                        endfont = ''
                    label += br + startfont + med + endfont
                style, penwidth = '', 1
                if label == "":
                    label = "(none)"
                    style = ',style="dotted"'
                else:
                    style = ',style="rounded"'

                # Simple arc weight to give rough indication of most frequent nodes
                count = sum([qq.pat_ids[pat]["count"] for pat in qq.pat_ids])
                label += " (" + str(count) + ')'  # Note count on state
                if q_max == q_min or count < (q_max - (q_min - 1)) / 3:
                    penwidth = ",penwidth=1"
                elif count < (q_max - (q_min - 1)) / 3 * 2:
                    penwidth = ",penwidth=2"
                else:
                    penwidth = ",penwidth=3"

                durations = flat_durations(qq.pat_ids)
                out_dur = sum([end - start for (start, end) in durations])
                if durtype == "mean":
                    out_dur /= len(durations)

                xlabel = "{:d}d".format(int(out_dur / secs_day))
                print('  ', node, '[label=<' + label + '>,xlabel="' + xlabel + '"' + style + penwidth +
                      color + '];', file=fid)

            else:
                qq.print(durtype="total")

        for tt in self.transitions:
            if ptype == "dot":
                node_fr = tt.q_fr.id
                node_to = tt.q_to.id
                penwidth = ''

                # Arc weights to give rough indication of most frequent paths.
                if t_max == t_min or tt.count < (t_max - (t_min - 1)) / 3:
                    penwidth = ",penwidth=1"
                elif tt.count < (t_max - (t_min - 1)) / 3 * 2:
                    penwidth = ",penwidth=2"
                else:
                    penwidth = ",penwidth=3"
                print(' ', node_fr, "->", node_to, "[label=" +
                      '{:.3f}'.format(tt.count).rstrip('0').rstrip('.') +
                      ",fontcolor=red" + penwidth + ']',
                      file=fid)
            else:
                tt.print()

        if ptype == "dot":
            print('}', file=fid)
            if dotfile:
                fid.close()
                pdffile = dotfile
                if bFoundStInt:
                    pdffile += "_ST_INT"
                elif bFoundInt:
                    pdffile += "_INT"
                pdffile += ".eps"
                cmd = "dot -Teps -o " + pdffile + ' ' + dotfile + ".dot"
                system(cmd)

        return

    def print_meds(self, b_print=True):
        """Simple print or accumulation of state information (medications) per patient.

        NB: This is currently only valid for single per-patient models.

        Includes durations.  Returns patient-state information and interation-occurence information
        for accumulation and reporting.

        :param b_print: boolean whether to print or simply accumulate
        :return meds_list_pdfa:
        :return ints_list_pdfa: full details/state
        """
        meds_list_pdfa = []
        ints_list_pdfa = []
        if len(list(self.patients.keys())) > 1:
            print("WARNING: printing meds is not valid for models from more than one patient.")
        site, pat_id = list(self.patients.keys())[0].split('_')

        for qq in self.states:
            state_meds = sorted(flat_meds(qq.meds))  # HTML format output to show clashes
            meds = []
            ints = []
            st_ints = []

            if b_print:
                print(site + ', ' + pat_id, end=', ')
            for ii in range(len(state_meds)):
                med = state_meds[ii]
                hl_med = med  # annotate interactions for output
                meds.append(med)
                if med in [a[i] for a in qq.ints for i in [0,1]]:
                    hl_med = "///" + hl_med + "///"
                    ints.append(med)

                if med in [a[i] for a in qq.st_ints for i in [0,1]]:
                    hl_med = "***" + hl_med + "***"
                    st_ints.append(med)

                if b_print:
                    if ii > 0:
                        print(", ", end='')
                    print(hl_med, end='')

            duration = sum([end - start for (start, end) in flat_durations(qq.pat_ids)])

            for int_pr in qq.ints:
                ints_list_pdfa.append([int_pr, True, False, patientid, duration])

            for int_pr in qq.st_ints:
                ints_list_pdfa.append([int_pr, False, True, patientid, duration])

            if b_print:
                print(", ", duration, end='')
                print()

            meds_list_pdfa.append([pat_id, site, meds, ints, st_ints, qq.ints, qq.st_ints, duration])

        return meds_list_pdfa, ints_list_pdfa

    def print_stats(self, n_patient, b_print=True):
        """Output polypharmacy stats.

        patient ID, #prescs, #severe_interacts, #interacts, duration_stats, {rpt. for unique combinations)
        Initially, assuming per-patient sequence PPTA.

        Note: this is only effective for single-patient pdfa.

        :param n_patient: patient number for stats list
        :param b_print: boolean whether to print or simply return.

        :return: stats array appended with stats for these patients
        """
        # Initialise collectors - dictionary keyed on patientid (initially one only)
        stats = {}
        accum_stats = []  # Stats for return

        # Parse PPTA from root, accumulating stats
        st_q = []  # Stack of states processed

        # Push the first state
        for qq in self.q0:
            st_q.append(qq)

        # Explore the stack until it is empty
        while len(st_q) > 0:
            qq = st_q.pop()  # Pop a state

            # Process the state add the next set of states
            for tt in qq.t_out:
                st_q.append(tt.q_to)  # Add the destination PPTA states

            qmeds = tuple(sorted(flat_meds(qq.meds)))
            pat_id = list(qq.pat_ids)[0]

            if pat_id not in stats:
                stats[pat_id] = {}
                stats[pat_id]["n_events"] = self.patients[pat_id]  # raw no. prescription events
                stats[pat_id]["n_prescr"] = 0  # episodes = path len = set at leaf
                stats[pat_id]["uq_meds"] = []  # Unique medications prescribed
                stats[pat_id]["uq_prescrs"] = []  # Unique prescription combinations
                stats[pat_id]["n_ints"] = 0  # We will probably want to store the actual ints?
                stats[pat_id]["n_st_ints"] = 0
                stats[pat_id]["uq_ints"] = []  # Unique interactions
                stats[pat_id]["uq_st_ints"] = []  # Unique interactions
                stats[pat_id]["n_durs"] = 0
                stats[pat_id]["min_dur"] = secs_year * 999
                stats[pat_id]["tot_dur"] = 0
                stats[pat_id]["max_dur"] = 0

            if len(qmeds) > 0:
                stats[pat_id]["n_prescr"] += 1  # sum of steps on path
                stats[pat_id]["n_ints"] += len(qq.ints)
                stats[pat_id]["n_st_ints"] += len(qq.st_ints)

                durations = flat_durations(qq.pat_ids)
                stats[pat_id]["n_durs"] += len(durations)
                stats[pat_id]["tot_dur"] += sum([end - start for (start, end) in durations])
                min_dur = min([end - start for (start, end) in durations])
                if min_dur < stats[pat_id]["min_dur"]:
                    stats[pat_id]["min_dur"] = min_dur
                max_dur = max([end - start for (start, end) in durations])
                if max_dur > stats[pat_id]["max_dur"]:
                    stats[pat_id]["max_dur"] = max_dur

                for med in qmeds:
                    if med not in stats[pat_id]["uq_meds"]:
                        stats[pat_id]["uq_meds"].append(med)
                if set(qmeds) not in stats[pat_id]["uq_prescrs"]:
                    stats[pat_id]["uq_prescrs"].append(set(qmeds))
                for interact in qq.ints:
                    if interact not in stats[pat_id]["uq_ints"]:
                        stats[pat_id]["uq_ints"].append(interact)
                for st_int in qq.st_ints:
                    if st_int not in stats[pat_id]["uq_st_ints"]:
                        stats[pat_id]["uq_st_ints"].append(st_int)

        for pat_id in stats:
            output_site, output_pat = pat_id.split('_')
            if b_print:
                print(output_site + ',' + output_pat +
                      ',' + str(stats[pat_id]["n_events"]) +
                      ',' + str(stats[pat_id]["n_prescr"]) +
                      ',' + str(len(stats[pat_id]["uq_meds"])) +
                      ',' + str(len(stats[pat_id]["uq_prescrs"])) +
                      ',' + str(stats[pat_id]["n_ints"]) +
                      ',' + str(stats[pat_id]["n_st_ints"]) +
                      ',' + str(len(stats[pat_id]["uq_ints"])) +
                      ',' + str(len(stats[pat_id]["uq_st_ints"])) +
                      ',' + "{:0.2f}".format(stats[pat_id]["min_dur"] / secs_day) +
                      ',' + "{:0.2f}".format(stats[pat_id]["tot_dur"] / stats[pat_id]["n_durs"] / secs_day) +
                      ',' + "{:0.2f}".format(stats[pat_id]["max_dur"] / secs_day)
                  )
            accum_stats.append([
                n_patient,
                output_site,
                output_pat,
                stats[pat_id]["n_events"],
                stats[pat_id]["n_prescr"],
                len(stats[pat_id]["uq_meds"]),
                len(stats[pat_id]["uq_prescrs"]),
                stats[pat_id]["n_ints"],
                stats[pat_id]["n_st_ints"],
                len(stats[pat_id]["uq_ints"]),
                len(stats[pat_id]["uq_st_ints"]),
                stats[pat_id]["min_dur"] / secs_day,
                stats[pat_id]["tot_dur"] / stats[pat_id]["n_durs"] / secs_day,
                stats[pat_id]["max_dur"] / secs_day,
                stats[pat_id]["uq_meds"],  # Temp to obtain / check full stats
                stats[pat_id]["uq_prescrs"],
                stats[pat_id]["uq_ints"],
                stats[pat_id]["uq_st_ints"]
            ])
        return accum_stats

    # Sub-classes of the S-PDFA class.
    class State(object):
        """State in a state-emitting PDFA (Marks a unique pharmacy combination)."""
        def __init__(self, q_id, meds, ints, st_ints, pat_ids=None):
            """Initialise a state in a PDFA.

            :param q_id: ID of the new state (convention: q{int}),
            :param meds: set of medication represented by this transition
            :param ints: pairs of standard medication interactions in this medication group
            :param st_ints: pairs of strong medication interactions in this medication group
            :param pat_ids: List of (or single) patient ID(s).
            """
            self.id = q_id
            self.meds = meds  # Set of medications which represents the symbol on this transition
            self.ints, self.st_ints = ints, st_ints  # no. of interactions previously counted
            self.pat_ids = pat_ids  # Stats for patient prescription traces which visited the state
            # Including counts and durations - assuming copied from PDFA.

            self.t_out = []  # For ease of parsing
            self.t_in = []

        def __lt__(self, qq):
            """Define state inequality based on string ID."""
            return self.id < qq.id

        def print(self, durtype="mean"):
            durations = flat_durations(self.pat_ids)
            out_dur = sum([end - start for (start, end) in durations])
            if durtype == "mean":
                out_dur /= len(durations)
            duration = "{:d}d".format(int(out_dur / secs_day))

            count = sum([self.pat_ids[pat]["count"] for pat in self.pat_ids])

            # Mark input and output states.
            if len(self.t_in) <= 0:
                print("[START] ", end='')
            if len(self.t_out) <= 0:
                print("[END] ", end='')
            print(self.id, '[', end='')
            b_first = True
            for med in sorted(self.meds):
                if not b_first:
                    print(", ", end='')
                else:
                    b_first = False
                print(med[0], end='')
            print(" (" + str(self.ints) + ',' + str(self.st_ints) + ')', end='')
            print('(' + str(count) + ')] ', end='')
            print(duration)

    class Trans(object):
        """Transition between state-emitting PDFA states (marks change from pharmacy status)."""

        def __init__(self, q_fr, q_to, count=0, pat_ids=None):
            """Initialise a transition between two PDFA states.

            :param q_fr: preceding state
            :param q_to: succeeding state
            :param count: How many times was this transition passed.
            :param pat_ids: List of (or single) patient ID(s).
            """
            if not isinstance(q_fr, S_PDFA.State) \
                    or not isinstance(q_to, S_PDFA.State):
                raise Exception("S-PDFA transition between invalid states.")

            self.id = q_fr.id + '_' + q_to.id  # May not be necessary
            self.q_fr = q_fr  # Source and destination states
            self.q_to = q_to
            self.count = count  # How many times was the transition passed?
            self.pat_ids = set()  # Set of patient prescription traces which visited the state
            if pat_ids:
                self.pat_ids = pat_ids
            q_to.t_in.append(self)
            q_fr.t_out.append(self)

        def print(self):
            node_fr = self.q_fr.id
            node_to = self.q_to.id
            print('--> ', node_fr, "->", node_to, "]")


def flat_durations(dict_pat_ids):
    """Extract list of (start, end) pairs from nested dictionary of patient stats (on arc or transition).

    :param dict_pat_ids: dictionary of patient stats including "durations" as found on PDFA transition or SPDFA state.
    """
    durations = [dict_pat_ids[pat]["durations"] for pat in dict_pat_ids]
    durations = [dd for durpairs in durations for dd in durpairs]
    return durations

def flat_meds(meds, pp=False):
    """Extract list of med names from list of (med, freq) pairs

    :param meds:
    :param pp: output printable version (string)
    """
    flat = [med[0] for med in meds]
    if pp:
        flat = '"' + ",".join(sorted(flat)) + '"'
    return flat

def output_events(events):
    """Pretty output of event sequences, for debugging.

    :param events: The events to display.
    """
    for event in events:
        # Format the epoch timestamps
        event_date = time.strftime("%d-%b-%Y", time.localtime(event[0]))
        paired_event_date = time.strftime("%d-%b-%Y", time.localtime(event[3]))
        print(event_date, event[1], event[2], paired_event_date)

def pre_process_events(events):
    """Adjust ordered sequence of start/end prescription events for a patient.

    :param events: the raw event sequence.

    - Remove single-day prescriptions (not at present)?
    - Merge contiguous prescriptions of the same medication?
    - Change multiple equal prescriptions (med & duration) to sequential?
    """
    # 1.  Remove single-day prescriptions
    # 2.  Treat a sequence of same medication issued on same date as a set of (e.g.) boxes.
    #     Convert start/end dates so simulate them being used in sequence.
    # (We need to have stored both start and end dates in each event, so we can find those to adjust/remove.)
    # for all on same start date with same medication
    #     remove all start events but the first
    #     remove all relevant end events
    #     add new end event for the sum of the durations.
    for ee in events:
        if ee[e_se] == start_tag:  # start event
            date_issue = ee[e_event_date]
            med = ee[e_med]
            event_set = [e for e in events if e[e_se] == start_tag and e[e_event_date] == date_issue and e[e_med] == med]
            if len(event_set) > 1:  # more than one same-date prescription for this medication
                new_date_issue = date_issue
                new_end_date = new_date_issue  # to defer with the multiple durations

                for start_event in event_set:
                    if DEBUG:
                        print("removing", (time.strftime("%d-%b-%Y", time.localtime(start_event[e_event_date])), start_tag,
                                           start_event[e_med],
                                           time.strftime("%d-%b-%Y", time.localtime(start_event[e_pair_date]))))
                        print("removing", (time.strftime("%d-%b-%Y", time.localtime(start_event[e_pair_date])), end_tag,
                                           start_event[e_med],
                                           time.strftime("%d-%b-%Y", time.localtime(start_event[e_event_date]))))

                    # Add one to adjust for completing prescription at the end of the specified end date.
                    new_end_date += (start_event[e_pair_date] - start_event[e_event_date]) + secs_day

                    events.remove(start_event)
                    events.remove((start_event[e_pair_date], end_tag, start_event[e_med], start_event[e_event_date]))  # matching end event

                if DEBUG:
                    print("adding", (time.strftime("%d-%b-%Y", time.localtime(new_date_issue)), start_tag, med,
                                     time.strftime("%d-%b-%Y", time.localtime(new_end_date))))
                    print("adding", (time.strftime("%d-%b-%Y", time.localtime(new_end_date)), end_tag, med,
                                     time.strftime("%d-%b-%Y", time.localtime(new_date_issue))))
                bisect.insort_right(events, (new_date_issue, start_tag, med, new_end_date))
                bisect.insort_left(events, (new_end_date, end_tag, med, new_date_issue))

    # 3.  Treat a contiguous sequence of prescriptions for the same medication as one longer
    #     prescription period (remove intermediate start/end dates on the same day or next.
    #     This ignores early prescription (start of next is before start of current.
    #     In fact we need do nothing; this will be handled in the conversion to multi-medication states.

    return events

def events_seq_ppta(events, pat_id, n_raw_events, rm_no_meds=False):
    """Build a PPTA from a sequence of patient prescription events.

    Initially this will be a sequence, for a given patient.
    Eventually it will be a full PPTA.

    :param events: adjusted (pre_process_events) sequence of events
    :param pat_id: patient ID
    :param n_raw_events: no. of prescription events for this patient in the original data
    :param rm_no_meds: remove transitions where no medication was being taken
    """
    ppta = PDFA(pat_id, n_raw_events)

    # Parse a single sequence of events
    # Create a transition to a new state whenever the combination of medications changes.
    # - ignore multiple concurrent prescription of same medication (assume new prescription overlap);
    # - ignore sequential end/start events for the same medication (*should* occur contiguously).

    # set of pairs (med, count) to account for multiple parallel prescription, else end events will be incorrect.
    current_meds = set()
    curr_state = ppta.q0
    curr_trans = None

    for event in events:
        med_pair = [med for med in current_meds if med[0] == event[e_med]]
        if med_pair:
            med_pair = med_pair[0]
        if event[e_se] == start_tag:
            # If change to current meds (which allows for repeated prescription of the same medication)
            # Create new state and transition.
            # Ignore repeated prescriptions of the same medication.
            if DEBUG:
                print("Add trans? ", end='')
                print(event[e_med], flat_meds(current_meds), end=' ')
            if event[e_med] not in flat_meds(current_meds):
                if DEBUG:
                    print("yes")
                current_meds.add((event[e_med], 1))  # add medication with a count of 1
                curr_state, curr_trans = \
                    ppta.add_pdfa_trans_state(curr_state, curr_trans, current_meds, pat_id, event[e_event_date], output_log=False)
            else:
                if DEBUG:
                    print("no - incr", med_pair)
                current_meds.remove(med_pair)
                med_pair = (med_pair[0], med_pair[1]+1)
                current_meds.add(med_pair)
        else:  # end events
            # Reduce the count of the medication, creating new state if it drops to zero.
            if DEBUG:
                print(med_pair)
            new_count = med_pair[1] - 1
            current_meds.remove(med_pair)
            if new_count > 0:
                current_meds.add((med_pair[0], new_count))
            else:
                curr_state, curr_trans = \
                    ppta.add_pdfa_trans_state(curr_state, curr_trans, current_meds, pat_id, event[e_event_date], output_log=False)
            if DEBUG:
                print("decr", med_pair, current_meds)

    # If final transition is empty, remove it, else add an end date
    if len(current_meds) <= 0:
        ppta.delete_trans_state(curr_trans)
    else:
        curr_trans.set_end(pat_id, event[e_event_date])

    # Remove empty transitions
    # only valid for sequence PPTA: avoid adding in the first place.
    ppta.remove_empty_SeqPPTA(rm_no_meds)

    # Ditto.
    ppta.remove_dups_SeqPPTA()

    # Do we still need to check for duplicate states, if we ended one med then recreated it in the next step?
    return(ppta)

def uniq(lst):
    """Return generator for unique ordered list.

    Use as list(uniq(l)).
    Courtesy of https://stackoverflow.com/questions/13464152/typeerror-unhashable-type-list-when-using-built-in-set-function
    """
    last = object()
    for item in lst:
        if item == last:
            continue
        yield item
        last = item

def get_interactions():
    """Retrieve list of interactions from the Neo4j database.

    To speed debugging: load from local pickle if present.

    If database is not running (e.g. on localhost:7474) then return empty
    We retrieve all the interactions (around 5000) for local processing rather than querying the datbase each time.

    :returns lists: interacts, strongly_interacts
    """
    b_running, r_ints, r_st_ints = False, [], []

    PKL = "neo4j_interactions.pkl"

    # Load local cache if present
    if path.exists(PKL):
        print("Reading cached BNF interactions from disk.  Remove", PKL, "to revert to Neo4j.")
        with open(PKL, 'rb') as fid:
            s_ints = pickle.load(fid)
            s_st_ints = pickle.load(fid)
            b_running = True

    else:
        try:
            print("Loading and sorting BNF interactions from Neo4j.")
            graph = Graph(password=NEO_PASS)

            d_ints = graph.data("""
                MATCH (comm1:DRUG)-[s1:SYNONYM *0..]->(gen1:DRUG)-[b1:BELONGS *0..]->
                      (grp1:DRUG)-[i:INTERACTS]->(grp2:DRUG)<-[b2:BELONGS *0..]-
                      (gen2:DRUG)<-[s2:SYNONYM *0..]-(comm2:DRUG)
                RETURN comm1.name AS comm1, gen1.name AS gen1, grp1.name AS grp1,
                       grp2.name AS grp2, gen2.name AS gen2, comm2.name AS comm2
            """)

            d_st_ints = graph.data("""
                MATCH (comm1:DRUG)-[s1:SYNONYM *0..]->(gen1:DRUG)-[b1:BELONGS *0..]->
                      (grp1:DRUG)-[i:STRONGLY_INTERACTS]->(grp2:DRUG)<-[b2:BELONGS *0..]-
                      (gen2:DRUG)<-[s2:SYNONYM *0..]-(comm2:DRUG)
                RETURN comm1.name AS comm1, gen1.name AS gen1, grp1.name AS grp1,
                       grp2.name AS grp2, gen2.name AS gen2, comm2.name AS comm2
            """)

            # NOTE: although labelled comm1 and comm2, if the interacting drug names are not the commercial names,
            #       then these will be the generic (or group).
            r_ints = list(uniq(sorted(
                [sorted([a["comm1"].lower(), a["comm2"].lower()]) for a in d_ints]
            )))
            r_st_ints = list(uniq(sorted(
                [sorted([a["comm1"].lower(), a["comm2"].lower()]) for a in d_st_ints]
            )))
            s_ints = set(tuple(a) for a in r_ints)
            s_st_ints = set(tuple(a) for a in r_st_ints)

            # Remove standard interactions which duplicate strong interactions
            s_ints.difference_update(s_st_ints)

            # Very basic sanity check
            if len(d_ints) <= 0:
                raise Exception("No interactions returned!")

            if len(d_st_ints) <= 0:
                raise Exception("No strong interactions returned!")

            b_running = True

            # Dump pickle for later use
            with open(PKL, 'wb') as output:
                pickle.dump(s_ints, output, pickle.HIGHEST_PROTOCOL)
                pickle.dump(s_st_ints, output, pickle.HIGHEST_PROTOCOL)

        except:
            print("Failed to connect to database!")
            b_running = False
            s_ints, s_st_ints = set(), set()

    return b_running, s_ints, s_st_ints

def chk_interactions(b_DB, ints, st_ints, medset):
    """Check list of medications for any interactions.

    :param b_DB: True if database is running
    :param ints: list of standard interactions
    :param st_ints: list of strong interactions
    :param medset: set of medications to check against ints and st_ints
    :returns list of pairs of (from,to) interactions.
    """
    # Get all pairs
    meds = flat_meds(medset)
    pairs_ints, pairs_st_ints = set(), set()
    if b_DB:
        # Unique list of medication pairs
        medpairs = list(uniq(sorted([sorted([a[0].lower(), a[1].lower()])
                                     for a in list(itertools.permutations(meds,2))]
                                    )))
        medpairs = set(tuple(a) for a in medpairs)

        for mp in medpairs:
            if mp in ints:
                pairs_ints.add(mp)
            if mp in st_ints:
                pairs_st_ints.add(mp)

    return list(pairs_ints), list(pairs_st_ints)

def gen_id():
    """Generate state IDs."""
    q_id = 0
    while True:
        yield q_id
        q_id += 1

def test_gen_overlap_width():
    """Basic generalisation tests.

    1. Merge "short"-running activities, up to quite long ones.
    2. Merge "small" med group activities, up to quite large groups.
    3. Merge "similar" activities, up to quite dissimilar ones.
    """
    ###
    sm_grp = 1
    sm_dur = 10
    subset = 1
    print("Generalise: remove small groups")
    sm_grps = np.arange(1,5)
    # sm_grps = [5, 10]  # 100 patient model
    ii = 0

    gppta = sppta  # copy.deepcopy(sppta)
    if dotfile:
        gppta.print("dot", patientid, dotfile + "_gen", durtype="sum")
        input()

    while ii < len(sm_grps):
        sm_grp = sm_grps[ii]
        gppta = sppta  # copy.deepcopy(sppta)
        gppta.generalise(thresh_sm_grp=sm_grp, thresh_sm_dur_d=sm_dur, thresh_merge_subset=subset)
        if dotfile:
            gppta.print("dot", patientid, dotfile + "_gen", durtype="sum")
        ii = getch(ii, len(sm_grps))  # Update
    maxgen_grp = sm_grp

    ###
    sm_grp = 1
    sm_dur = 10
    subset = 1
    print("Generalise: remove short duration nodes")
    sm_durs = np.arange(10,60,10)
    # sm_durs = [50, 100]  # 100 patient model
    ii = 0
    while ii < len(sm_durs):
        sm_dur = sm_durs[ii]
        gppta = sppta  # copy.deepcopy(sppta)
        gppta.generalise(thresh_sm_grp=sm_grp, thresh_sm_dur_d=sm_dur, thresh_merge_subset=subset)
        if dotfile:
            gppta.print("dot", patientid, dotfile + "_gen", durtype="sum")
        ii = getch(ii, len(sm_durs))  # Update
    maxgen_dur = sm_dur

    ###
    sm_grp = 1
    sm_dur = 10
    subset = 1
    print("Generalise: merge similar groups")
    subsets = np.arange(1, 0, -0.1)
    # subsets = [0.5, 0.2, 0.1]  # 100 patient model
    ii = 0
    while ii < len(subsets):
        if ii >= len(subsets):
            subset = subsets[-1]
        else:
            subset = subsets[ii]
        gppta = sppta  # copy.deepcopy(sppta)
        gppta.generalise(thresh_sm_grp=sm_grp, thresh_sm_dur_d=sm_dur, thresh_merge_subset=subset)
        if dotfile:
            gppta.print("dot", patientid, dotfile + "_gen", durtype="sum")
        ii = getch(ii, len(subsets))  # Update
    maxgen_subset = subset

    ###
    print("Generalise: max small group, duration, overlap")
    gppta = sppta  # copy.deepcopy(sppta)
    gppta.generalise(thresh_sm_grp=maxgen_grp, thresh_sm_dur_d=maxgen_dur,
                     thresh_merge_subset=maxgen_subset)
    if dotfile:
        gppta.print("dot", patientid, dotfile + "_gen", durtype="sum")

def test_gen_iterate():
    """Basic generalisation: iterate e.g. 80% overlap.

    1. Merge "short"-running activities, up to quite long ones.
    2. Merge "small" med group activities, up to quite large groups.
    3. Merge "similar" activities, up to quite dissimilar ones.
    """
    sm_grp, sm_dur = 2, 10  # These work together to remove "small" nodes
    subset = .7  # overlap to merge nodes

    print("Generalise: remove small groups")
    ii = 0
    removed = [None]
    gppta = sppta  # copy.deepcopy(sppta)
    if dotfile:
        gppta.print("dot", patientid, dotfile + "_gen", durtype="sum")
        input()

    while len(removed) > 0:  # Iterate while changes are still being made
        removed = gppta.generalise(thresh_sm_grp=sm_grp, thresh_sm_dur_d=sm_dur, thresh_merge_subset=subset)
        # print(removed)
        if dotfile:
            gppta.print("dot", patientid, dotfile + "_gen", durtype="sum")
        ii = getch(ii, 0)  # Update


if __name__ == "__main__":
    """Main processing.

    Some options for generalisation and visualisation are commented in the code below.
    """
    # Parse the arguments
    parser = argparse.ArgumentParser(description="Development Process Mining algorithm for prescription data.")
    parser.add_argument('-f', '--csvfile', dest="csvfile", default=None,
                        help="CSV file output from prep_data.py.")
    parser.add_argument('-e', '--eventlog', dest="logfile", default=None,
                        help="Consolidated event log file to write to")
    parser.add_argument('-p', '--patient', dest="tst_patientid", default=None,
                        help="Patient ID.")
    parser.add_argument('-d', '--dotfile', dest="dotfile", default=None,
                        help="DOT file output.")
    parser.add_argument('-x', '--debug', dest="debug", action="store_true", default=False)
    parser.add_argument('-n', '--noprint', dest="noprint", help="No output",
                        action="store_true", default=False)
    parser.add_argument('-m', '--nomultistart', dest="multistart", action="store_true", default=False)

    args = parser.parse_args()

    f_csv = ""
    if args.csvfile:
        f_csv = args.csvfile
    else:
        print("No file supplied!")
        exit(1)
    if args.dotfile:
        dotfile = args.dotfile
    else:
        dotfile = pdffile = None
    if args.logfile:
        logfile = args.logfile
        try:
            fidlog = open(logfile, 'w')
        except:
            print("Error opening event log file")
            raise
    else:
        logfile = fidlog = None
    if args.tst_patientid:
        tst_patientid = args.tst_patientid
    else:
        tst_patientid = None
    if args.multistart:
        multistart = False
    else:
        multistart = True
    if args.debug:
        DEBUG = True
    if args.noprint:
        NOPRINT = True

    # Attempt to retrieve interactions from database.
    b_DB, ints, st_ints = get_interactions()

    # Read in the CSV
    recs = []  # store read records
    ii = 0
    patients = set()  # for count
    with open(f_csv, newline='', mode='r') as fid:
        reader = csv.reader(fid, delimiter=',', quotechar='"')
        line = ""
        try:
            for line in reader:
                if ii > 0:  # skip the header
                    patients.add(line[ANON_ID])
                    try:
                        ep_date_issue = int(time.mktime(time.strptime(line[DATE_ISSUE], patt_date_issue)))
                    except ValueError:  # Excel export problem - seems to be epoch already
                        ep_date_issue = int(line[DATE_ISSUE])
                    try:
                        ep_end_date = int(time.mktime(time.strptime(line[END_DATE], patt_end_date)))
                    except ValueError:
                        ep_end_date = int(line[END_DATE])

                    # Modify 0-day prescriptions to 1-day
                    if B_ZERO_TO_1 and ep_end_date <= ep_date_issue:
                        ep_end_date = ep_date_issue + secs_day
                    recs.append([line[ANON_ID], ep_date_issue, line[MED], ep_end_date])
                ii += 1
        except Exception as e:
            print("ERROR READING CSV (last line):", ii, file=sys.stderr)
            print(line)
            raise e

    gen_id = gen_id()  # Initialise ID generator

    # Sort by patient ID and date of issue
    recs.sort(key=operator.itemgetter(ANON_ID,DATE_ISSUE))
    n_patients = len(patients)

    if not NOPRINT:
        print("Total Patients,Index,Site ID,Patient ID,Prescription Events,Consolidated Prescriptions," +
              "Unique Medications,Unique Prescriptions,Standard Interactions,Strong Interactions," +
              "Unique Standard Interactions,Unique Strong Interactions," +
              "Min Days,Mean Days,Max Days")
    else:
        print("Saving stats to structures for use in Jupyter")
    stats_hdr = [
        "Index",
        "Site ID",
        "Patient ID",
        "Prescription Events",
        "Consolidated Prescriptions",
        "Unique Medications",
        "Unique Prescriptions",
        "Standard Interactions",
        "Strong Interactions",
        "Unique Standard Interactions",
        "Unique Strong Interactions",
        "Min Days",
        "Mean Days",
        "Max Days",
        "Unique Meds",
        "Unique Prescrs",
        "Unique Std Ints",
        "Unique Strong Ints"
    ]

    # Go through patients
    patientid = recs[0][0]
    n_patient = 0
    pat_events = []  # build list of start and end prescription events
    ppta = PDFA()  # Accumulate per-patient models
    stats = []  # Stats per patient
    meds_list = []  # Meds list for each patient, std, strong ints, durations
    ints_list = []  # (Duplicates the above; med pr -> std/strong, patient count, prescr count, dur
    for ii in range(len(recs)):
        rec = recs[ii]
        patientid = rec[0]

        # Accumulate the events for this record
        if not tst_patientid or rec[0] == tst_patientid:  # Select patient ID if required
            # Insert events for start and end prescription
            ep_date_issue = rec[DATE_ISSUE]
            ep_end_date = rec[END_DATE]
            bisect.insort_right(pat_events, (ep_date_issue, start_tag, rec[MED], ep_end_date))  # store end date for matching
            bisect.insort_left(pat_events, (ep_end_date, end_tag, rec[MED], ep_date_issue))

        if ii+1 >= len(recs) or recs[ii+1][0] != patientid:  # At end of each patient's records
            if not tst_patientid or patientid == tst_patientid:  # Select patient ID if required
                n_patient += 1

                # Pre-process events (merging contiguous prescriptions etc.
                n_raw_events = len([p[1] for p in pat_events if p[1] == start_tag])
                pat_events = pre_process_events(pat_events)

                # Generate per-patient "sequence" PPTA
                seq_ppta = events_seq_ppta(pat_events, patientid, n_raw_events, rm_no_meds=False)
                if logfile:
                    # Write to consolidated event log
                    seq_ppta.print("eventlog", pat_id=patientid, n_patient=n_patient, fidlog=fidlog)
                ppta.merge(seq_ppta)

                if not NOPRINT:
                    print(str(n_patients) + ',' + str(n_patient) + ',', end='')
                # ppta.print_stats()  # Output polypharmacy stats for this patient.
                seq_sppta = S_PDFA(seq_ppta, multistart)  # convert to state-emitting - for visusalisation.

                # (Duplicate) Output polypharmacy stats for this patient.
                stats += seq_sppta.print_stats(n_patient, b_print=(not NOPRINT))  # Append the stats

                # Output/accumulate medication list for this patient
                # Do this here to capture duplicate states
                meds_list_pdfa, ints_list_pdfa = seq_sppta.print_meds(b_print=(not NOPRINT))
                meds_list += meds_list_pdfa
                ints_list += ints_list_pdfa
                # sppta.print()

                # if dotfile:
                #     seq_ppta.print("dot", patientid, dotfile + patientid + "_ppta")
                #     seq_sppta.print("dot", patientid, dotfile + patientid + "_sppta")

                # seq_sppta.ppta_pdfa_naive()  # naive PDFA removes state duplication and redirects arcs naively.
                # # sppta.print()
                #
                # if dotfile:
                #     seq_sppta.print("dot", patientid, dotfile + patientid + "_spdfa", "total")
            pat_events = []

    # Print the PPTA before any generalisation or conversion to state-emitting.
    # if dotfile:
    #     ppta.print(ptype="dot", pat_id=patientid, n_patient=n_patient, dotfile=dotfile+"_ppta", durtype="sum")

    # Convert the PPTA to state-emitting (for visualisation and naive generalisation) and print.
    # sppta = S_PDFA(ppta, multistart)
    # if dotfile:
    #     sppta.print(ptype="dot", pat_id=patientid, dotfile=dotfile+"_sppta", durtype="sum")

    # Convert the (state-emitting) PPTA naively to PDFA and print
    # sppta.ppta_pdfa_naive()  # Naive PDFA from the S_PPTA
    # sppta.pseudo_prob()
    # if dotfile:
    #     sppta.print(ptype="dot", pat_id=patientid, dotfile=dotfile+"_spdfa", durtype="sum")

    # # Try converting the generalised (transition-emitting) PDFA to state-emitting (and print)
    # appta = S_PDFA(ppta, multistart)  # ... and convert to S_PDFA
    # if dotfile:
    #     appta.print(ptype="dot", pat_id=patientid, dotfile=dotfile+"_apdfa", durtype="sum")

    yn = 'n'
    # print("Test generalisation?", end=' ')
    # yn = input()
    if yn == 'y' or yn == 'Y':
        # Testing generalisation / complexity reduction
        # First test generalisation up to including very small overlaps, long activities, etc.
        test_gen_overlap_width()
        # test_gen_iterate()

    if fidlog:
        fidlog.close()
