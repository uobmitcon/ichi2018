#!/usr/bin/env python
"""Web scrape the BNF database into Neo4j: for a commercial:generic drug name mapping.

Phil Weber 2017-10-23 First version.  Alternative website could be www.mims.co.uk.
o  c5m 50s for BNF.py to run (interactions extract).
o  c3m 40s for this to run (drug synonyms).
o  1579 generics found
o  337 missing after name translation/matching and excluding "with" (saved in missing.out).

To Do:
o  2018-05-09 BNF website has changed again; this no longer works.
"""

import argparse
import requests
import re
from lxml import html
from py2neo import Graph, Node, Relationship

NEO_PASS = "PASSWORD"  # Some kind of security needed

# The BNF has two lists of drugs: one for the interactions, the other the drug details.
# They are different.  Here we attempt to map drug names between the two.

# Specific name mappings.
NAME_MAP_SPECIFIC = [
    ["^hepatitis [ab].*vaccine$", "hepatitis vaccines"],
    ["^adrenaline/epinephrine$", "adrenaline (epinephrine)"],
    ["^noradrenaline/norepinephrine$", "noradrenaline (norepinephrine)"],
    ["^agalsidase .*$", "agalsidase alfa and beta"],
    ["^magnesium .*ate$", "magnesium salts (oral)"],
    ["^magnesium hydroxide$", "magnesium salts (oral)"],
    ["^calcium .*ate$", "calcium salts"],
    ["^calcium polystyrene solfonate$", "calcium salts"],
    ["^diphtheria antitoxin$", "diphtheria vaccines"],
    ["^anti-d (rh0) immunoglobulin$", "anti-d immunoglobulins"],
    ["^insulin .*$", "insulin"],
    ["^biphasic .*insulin .*$", "insulin"],
    ["^biphasic isophane insulin$", "insulin"],
    ["^darbepoetin alfa$", "darbepoetin"],
    ["^typhoid vaccine$", "typhoid vaccine (parenteral)"],
    ["^conjugated oestrogens (equine)$", "oestrogens, conjugated"],
    ["^epoetin .*$", "epoetins"],
    ["^meningococcal .*vaccine.*$", "meningococcal vaccines"],
    ["^pneumococcal polysaccharide .*vaccine (adsorbed)$", "pneumococcal vaccine"],
    ["^tick-borne encephalitis vaccine.*$", "tick-borne encephalitis vaccine"],
    ["^interferon gamma-1b$", "interferon gamma"],
    ["^interferon beta$", "interferons"],
    ["^iron .*$", "iron salts"],
    ["^perindopril .*$", "perindopril"],
    ["^testosterone.*$", "testosterone"]
]

# Most of the salts etc. need to be removed.  " {patt}$" will be removed.  2nd field is approx count.
# 3rd field is exceptions.
NAME_MAP_SUFFICES = [
    ["acetate", 17, []],
    ["sulfate", 17, "ferrous sulfate"],
    ["hydrochloride", 106, []],
    ["chloride", 11, ["potassium chloride"]],
    ["bromide", 12, []],
    ["tartrate", 7, []],
    ["decanoate", 6, []],
    ["maleate", 5, []],
    ["acetonide", 3, []],
    ["sodium", 27, ["colistimethate sodium", "risedronate sodium"]],
    ["dipropionate", 2, []],
    ["phosphate", 6, []],
    ["medoxomil", 2, []],
    ["dipivoxil", 1, []],
    ["cilexetil", 1, []],
    ["type a", 1, []],
    ["type b", 1, []],
    ["monohydrate", 1, []],
    ["besilate", 1, []],
    ["fumarate", 4, ["dimethyl fumarate", "ferrous fumarate"]],
    ["fosamil", 1, []],
    ["hydrate", 1, []],
    ["butyrate", 1, []],
    ["citrate", 1, []],
    ["carbonate", 1, []],
    ["elexilate", 1, []],
    ["mesilate", 3, []],
    ["potassium", 2, []],
    ["furoate", 2, []],
    ["trometamol", 1, []],
    ["hippurate", 1, []],
    ["mofetil", 1, []],
    ["embonate", 1, []],
    ["teoclate", 1, []],
    ["succinate", 1, []],
    ["disopraxil", 1, []],
    ["butylbromide", 1, []],
    ["hydrobromide", 1, []],
    ["dihydrochloride", 1, []],
    ["hydrobromide", 1, []],
    ["hexacetonide", 1, []]
]

# These are a mix of plural or not in both databases.  Check both and return match.
NAME_CONFUSED_PLURAL = [
    "vaccine",
    "extract"
]

# Clarity functions for interaction with Neo4j
class SYNONYM(Relationship):
    """Drugname A is a synonym for drug name B where either are commercial or generic."""
    pass


def extract_prodname(ss):
    """Extract product name from h4 title.

     Best endeavours:
     1. remove everything including and following the first number,
     2. lowercate
     3. strip whitespace.
     e.g. Aspire Hayfever Relief 2% eye drops (Aspire Pharma Ltd) --> Aspire Hayfever Relief.
     """

    ss_ = re.sub("[ ]*[0-9]+.*$", '', ss).lower()
    ss_ = re.sub("[ ]*tablets.*$", '', ss_)
    ss_ = ss_.strip()
    return ss_


def chk_generic_exists(ss):
    """Attempt to match supplied drug name (from BNF drugs list)

    (With the drugs list retrieved from BNF interactions, stored in the database).
    """
    # Strip unwanted suffices
    for repl in NAME_MAP_SUFFICES:
        if ss not in repl[2]:
            ss = re.sub(' ' + repl[0] + '$', '', ss)

    # Make pattern match changes
    for repl in NAME_MAP_SPECIFIC:
        ss = re.sub(repl[0], repl[1], ss)

    # Check plurals
    for base_plural in NAME_CONFUSED_PLURAL:
        if base_plural in ss:
            if base_plural + 's' in ss:  # plural form
                # match the plural
                if ss in generic_drugs:
                    return ss
                else:
                    # try the non-plural form
                    ss1 = re.sub(base_plural + 's', base_plural, ss)
                    if ss1 in generic_drugs:
                        return ss1
            else:
                # match the non-plural form
                if ss in generic_drugs:
                    return ss
                else:
                    # Try the plural form
                    ss1 = re.sub(base_plural, base_plural + 's', ss)
                    if ss1 in generic_drugs:
                        return ss1
                    # else continue

    # non-plurals
    if ss in generic_drugs:
        return ss
    else:
        return ''


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Web scraping the BNF database for medicinal forms (commercial names) for generic drugs.")
    parser.add_argument('-x', '--noDB', dest="noDB", action="store_true", default=False)

    args = parser.parse_args()
    if args.noDB:
        noDB = True
    else:
        noDB = False

    site = "https://bnf.nice.org.uk"

    url_druglist = site + "/drug"
    tree_druglist = html.fromstring(requests.get(url_druglist).content)

    # Attempt to connect to Neo4j database.  Must be running (e.g. on localhost:7474)
    if not noDB:
        try:
            graph = Graph(password=NEO_PASS)
        except:
            print("Failed to connect to database!")
            raise

    if not noDB:
        # Basic check whether the database is empty
        equals_rel = graph.data("MATCH p=()-[e:SYNONYM]->() RETURN e LIMIT 1")

        if equals_rel:
            graph.data("MATCH p=()-[r:SYNONYM]->() DELETE r")
            # raise Exception("Neo4j SYNONYM relation not empty!")

        # Retrieve list of drugs
        generic_drugs = graph.data('MATCH d=(n:DRUG) RETURN LOWER(n.name) AS n')
        generic_drugs = set([d['n'] for d in generic_drugs])


    alpha_sect = tree_druglist \
        .find("body") \
        .findall("div")[4] \
        .find("div") \
        .findall("section")

    for alpha in alpha_sect:
        druglist = alpha.find("div") \
            .findall("div")[1] \
            .find("ul") \
            .findall("li")

        for drug in druglist:
            generic_drug_name = str(drug.find("a").text_content()).lower()  # smooths out "span" structured contents
            href = drug.find("a").attrib["href"]  # URL for the drug details.

            print(generic_drug_name + ": ", end='')

            # Guess the URL to the medicinal forms, otherwise we need to go via the drug
            # details.
            url_medforms = site + '/medicinal-forms/' + href
            tree_medforms = html.fromstring(requests.get(url_medforms).content)

            # 1. Deal with direct interactions

            # This structure 2017-10-17
            # interact_div = tree_interactlist \
            #     .find("body") \
            #     .findall("div")[5] \  class="layout layout-secondary-wide topic-container"
            #     .findall("div")[1] \  class="content content-primary"
            #     .find("div") \  id={drug name} (approximately
            #     .find("div") \  class="interactions"
            #     .find("div")    class="interaction-list" (may be one? before this)
            drug_sects = tree_medforms \
                .find("body") \
                .findall("div")[5] \
                .find("div") \
                .find("div") \
                .findall("section")

            for drug_sect in drug_sects:
                drug_divs = drug_sect.findall("div")

                prod_names = set()  # Collect unique product names
                for drug_div in drug_divs:
                    if drug_div.attrib["class"] == "medicinalForm":
                        med_forms = drug_div.findall("div")

                        for med_prod_div in med_forms:
                            if med_prod_div.attrib["class"] == "medicinalProducts":

                                med_prods = med_prod_div.findall("div")

                                for med_prod in med_prods:
                                    prod_name = med_prod.find("h4").find("span").text_content()

                                    # Do our best to extract the product name
                                    prod_name = extract_prodname(prod_name)
                                    prod_names.add(prod_name)

            print(prod_names, end=' ')

            # Now insert the found names as synonyms
            # Check if target generic drug name exists and retrieve stored name.
            # Quotes arranged to match names with apostrophes, e.g. St. John's Wort.  
            generic_db_name = chk_generic_exists(generic_drug_name)

            if not generic_db_name:
                print("GENERIC NOT IN DB")
            else:
                print()

                for synonym in prod_names:
                    print("  ", synonym, end=' ')
                    if generic_db_name == synonym:
                        print("IGNORING", synonym)
                    else:
                        print()

                        # Now insert (merge) into Neo4j database
                        commercial_drug = Node("DRUG", name=synonym)
                        generic_drug = Node("DRUG", name=generic_db_name)
                        rel = SYNONYM(commercial_drug, generic_drug)
                        print("inserting", synonym, generic_db_name)

                        if not noDB:
                            graph.merge(commercial_drug)
                            graph.merge(rel)
