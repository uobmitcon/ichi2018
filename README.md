# README #

Code for the first stage of a process mining and text analytics approach to analysing data on medical
prescriptions (in the UK).  In support of a paper submitted to ICHI (International Conference on 
Healthcare Informatics) 2018.

### What is this repository for? ###

* Python code in support of the ICHI submission.

>
  1. Grammar (using [pyparsing](http://pyparsing.wikispaces.com/) to infer prescription end dates from
     unstructured "name, dose and quantity" text supplied by GPs.
  2. Code for web scraping (using [requests](http://docs.python-requests.org/en/master/)
     and [lxml](http://lxml.de/) the online [BNF database](https://bnf.nice.org.uk/).
  3. Code for mining and (naive) generalising 'prescription process' models and reporting statistics on
     polypharmacy.
  4. Jupyter notebook applying analysis to the retrieved statistics.
  
* Version 0.1
* **Note:** This is rough code developed for research purposes.  It has not particularly been optimised 
  nor developed to exhibit good Python style or efficiency, or to work beyond the bounds of the experiments
  explicitly carried out for this paper.  Some parts of it are only partially developed.

### How do I get set up? ###

Scrape interactions from the BNF website (needs re-writing following website change):

> ```./BNF.py > BNF.out```

Scrape commercial-generic name mappings (also needs re-write):

> ```./BNF_DrugNames.py > names.out```

Process raw prescription records to generate end dates:

> ```./stats_multisite.py -f prescrs.csv -o fail.csv -e err.csv enddates.csv 2>enddates.err```

Pre-process for process mining (reduce drug names):

> ```./prep_data.py -f enddates.csv [-p prob] > for_pm.csv```

Process mining:

> ```./mine_ppta.py -f for_pm.csv [-p patientID] [-d dotfileprefix]```

(May also be run from a Jupyter notebook.)

* Dependencies
  - Tested under Python 3.5.

### Contribution guidelines ###

* Not applicable.

### Who do I talk to? ###

* Phil Weber, School of Computer Science, University of Birmingham.  Moving to Aston University, May 2018.
* Mark Lee, School of Computer Science, University of Birmingham.
* Ian Litchfield, Institute of Applied Health Research, University of Birmingham.

**Updated 2018-05-09**