# Graph Database
Used to hold information about medication and their interactions.  We used Neo4J with the Cypher query language.

## Nodes
**DRUG** 
> Records drug names, both commercial and generic, and group names.


## Relations
DRUG --> **BELONGS** --> DRUG
> Maps drugs to groups.

DRUG --> **INTERACTS** --> DRUG
> Maps drug interactions

DRUG --> **STRONGLY_INTERACTS** --> DRUG
> Ditto strong interactions, as identified by the BNF

DRUG --> **SYNONYM** --> DRUG
> Maps generic to commercial drug names, as shown on the BNF.
