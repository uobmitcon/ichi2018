#!/usr/bin/env python
"""Basic pre-processing for output of stats.py/stats_multisite.py before process mining.

Reduce NDQ field to medication only (e.g. reduce modifiers for amount), to reduce number of distinct "events".
Phil Weber 2017-08-17
"""

import sys
import csv
import re
import argparse
import random


if __name__ == "__main__":
    # Parse the arguments
    parser = argparse.ArgumentParser(description="Pre-process for Process Mining.")
    parser.add_argument('-f', '--csvfile', dest="csvfile", default=None,
                        help="CSV file output from stats_multisite.py.")
    parser.add_argument('-p', '--prob', dest="p_select", type=float,
                        default=1.0, help="Probability of including patient.")

    args = parser.parse_args()

    f_csv = ""
    if args.csvfile:
        f_csv = args.csvfile
    else:
        print("No file supplied!")
        exit(1)
    if args.p_select:
        p_select = args.p_select
    else:
        print("Probability zero or invalid!")
        exit(1)

    # CSV output to stdout
    stdwriter = csv.writer(sys.stdout, delimiter=',', quotechar='"',
                           quoting=csv.QUOTE_MINIMAL)

    # Read in the CSV
    recs = []  # store read records
    patients = set()  # store a set of patients to include
    ii = 0
    with open(f_csv, newline='', mode='r') as fid:
        reader = csv.reader(fid, delimiter=',', quotechar='"')
        hdr = ["Anonymised Identifier", "Date of Issue", "Medication",
               "End Date"]
        line = ""
        try:
            for line in reader:
                ii += 1
                # Concatenate site and patient IDs
                rec = [line[0]+'_'+line[1], line[4], line[5], line[15]]
                recs.append(rec)
                patients.add(rec[0])
        except Exception as e:
            print("ERROR READING CSV (last line):", ii, file=sys.stderr)
            stdwriter.writerow(line)
            raise e

    # Retain a subset of patients, with the selected probability.
    if p_select < 1.0:
        random.seed()
        for patient in list(patients):
            p = random.random()
            if p >= p_select:
                patients.remove(patient)

    # Output CSV, with appropriate modifications
    stdwriter.writerow(hdr)
    for rec in recs[1:]:
        if rec[0] in patients:
            # Extract medication name from "Name, Dosage and Quantity" field.
            # Perhaps pyparsing needed.
            # Note: ignore differences in (e.g.) tablet size.
            ndq = rec[2].lower()
            ndq = re.sub(
                " ([0-9]|vaccine|with|tablets|caplet|chewable|oral|compact|sachets|patches|aqueous|capsules|granules|advance|one|compound|soup|post-op|suppositories|effervescent|sugar|dressing).*$",
                "", ndq).strip()
            rec[2] = ndq

            stdwriter.writerow(rec)
