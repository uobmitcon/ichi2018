#!/usr/bin/env python
"""Web scrape the BNF database of drug interactions into Neo4j.

Phil Weber 2017-03-17 updated from original Java version.
           2071-09-13 updated for new BNF website layout.

To Do:
o  2018-05-09 BNF website has changed again; this no longer works.
"""

import argparse
import requests
import re
from lxml import html
from py2neo import Graph, Node, Relationship

IL_STANDARD = "STANDARD"
IL_DANGER = "DANGER"
NEO_PASS = "PASSWORD"  # Some kind of security needed


# Clarity functions for interaction with Neo4j
class INTERACTS(Relationship):
    """Drug A interacts with drug B (directional in Neo4j but assumed non-directional.)"""
    pass


class STRONGLY_INTERACTS(Relationship):
    """Drug A strongly interacts with drug B (dir. in Neo4j but assumed non-dir.)"""
    pass


class BELONGS(Relationship):
    """Class of drug(s) A belongs to class of drugs B (directional)"""
    pass


def add_belongs(interact_div):
    """Add belongs relationships"""
    inherit_divs = interact_div.findall("h3")
    for inherit_div in inherit_divs:
        names = inherit_div.findall("strong")
        belonger_name = names[0].text_content().lower()
        belongsto_name = names[1].text_content().lower()
        belonger = Node("DRUG", name=belonger_name)
        belongsto = Node("DRUG", name=belongsto_name)
        belongs = BELONGS(belonger, belongsto)

        print("  ", belonger_name, "BELONGS TO", belongsto_name)

        if not noDB:
            graph.merge(belonger)
            graph.merge(belongsto)
            graph.merge(belongs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Web scraping for the BNF database of drug interactions.")
    parser.add_argument('-x', '--noDB', dest="noDB", action="store_true", default=False)

    args = parser.parse_args()
    if args.noDB:
        noDB = True
    else:
        noDB = False

    site = "https://bnf.nice.org.uk"

    url_druglist = site + "/interaction"
    tree_druglist = html.fromstring(requests.get(url_druglist).content)

    # Attempt to connect to Neo4j database.  Must be running (e.g. on localhost:7474)
    if not noDB:
        try:
            graph = Graph(password=NEO_PASS)
        except:
            print("Failed to connect to database!")
            raise

    if not noDB:
        # Basic check whether the database is empty
        drugs = graph.data("MATCH d=() RETURN d LIMIT 1")
        conds = graph.data("MATCH p=()-[i]->() RETURN i LIMIT 1")

        if drugs or conds:
            raise Exception("Neo4j database not empty!")
            # graph.data("MATCH p=()-[i]->() DELETE p")  # relations first
            # graph.data("MATCH d=() DELETE d")  # nodes last

    alpha_sect = tree_druglist \
        .find("body") \
        .findall("div")[4] \
        .find("div") \
        .findall("section")

    for alpha in alpha_sect:
        druglist = alpha.find("div") \
            .findall("div")[1] \
            .find("ul") \
            .findall("li")

        for drug in druglist:
            drug_name = str(drug.find("a").text_content()).lower()  # smooths out "span" structured contents
            href = drug.find("a").attrib["href"]

            print(drug_name + ": ", end='')

            url_interactlist = url_druglist + '/' + href
            tree_interactlist = html.fromstring(requests.get(url_interactlist).content)

            # 1. Deal with direct interactions

            # This structure 2017-10-17
            # interact_div = tree_interactlist \
            #     .find("body") \
            #     .findall("div")[5] \  class="layout layout-secondary-wide topic-container"
            #     .findall("div")[1] \  class="content content-primary"
            #     .find("div") \  id={drug name} (approximately
            #     .find("div") \  class="interactions"
            #     .find("div")    class="interaction-list" (may be one? before this)
            interact_div = tree_interactlist \
                .find("body") \
                .findall("div")[5] \
                .findall("div")[1] \
                .find("div") \
                .find("div")

            interact_divs = interact_div.findall("div")

            if interact_divs is None or len(interact_divs) <= 0:
                # Appears to indicate no interactions (not sure if we saw any of these?)
                print("EMPTY interactions div")
            elif len(interact_divs) == 1 and interact_divs[0].attrib["class"] == "interaction-list-inherited":
                print("ONLY inherited interactions")
            else:
                print()

            for interact_div in interact_divs:
                # Account for one anomaly in the web page layout; extra "alert" div block before interactions
                if interact_div.attrib["class"] == "alert alert-block alert-info":
                    True  # skip
                elif interact_div.attrib["class"] == "interaction-list":
                    interact_divs = interact_div.findall("div")

                    if interact_divs is None or len(interact_divs) <= 0:
                        # Appears to indicate no interactions
                        print("EMPTY interaction-list")
                        continue

                    for interact in interact_divs:
                        in_drug = interact \
                            .find("div") \
                            .find("div") \
                            .find("h4").text_content()
                        if not in_drug:
                            in_drug = drug_name.lower()  # seems to be the format for self-interactions
                        else:
                            in_drug = str(in_drug).lower()

                        in_target = interact.attrib["class"].split(' ')[-1]  # interaction scroll-target
                        if in_target == "NotSet":
                            in_level = IL_STANDARD
                        elif in_target == "High":
                            in_level = IL_DANGER
                        else:
                            print("  UNKOWN TARGET", in_target)

                        in_text = interact \
                            .find("div") \
                            .findall("div")[1] \
                            .find("div") \
                            .find("div").text_content()

                        in_text = re.sub(" ,", "",
                                         re.sub("^ [ ]*| [ ]*$", "",
                                                re.sub(" [ ]*", " ",
                                                        re.sub("\n[ ]*"," ", str(in_text)))))

                        try:
                            print("  " + in_drug + ": \"" + in_text + '\": ' + in_level)
                        except:
                            print(interact.text_content())
                            raise

                        # Now insert into Neo4j database
                        fr_drug = Node("DRUG", name=drug_name)
                        to_drug = Node("DRUG", name=in_drug)
                        if in_level == IL_STANDARD:
                            in_rel = INTERACTS(fr_drug, to_drug, text=in_text)
                        else:
                            in_rel = STRONGLY_INTERACTS(fr_drug, to_drug, text=in_text)

                        if not noDB:
                            graph.merge(fr_drug)
                            graph.merge(to_drug)
                            graph.merge(in_rel)

                # 2. Deal with inherited interactions (drug is a member of group)
                elif interact_div.attrib["class"] == "interaction-list-inherited":
                    add_belongs(interact_div)

            # 3. Deal with inherited interactions (drugs that are a member of this group)
            # This structure 2017-10-17
            # interact_div = tree_interactlist \
            #     .find("body") \
            #     .findall("div")[5] \  class="layout layout-secondary-wide topic-container"
            #     .findall("div")[1] \  class="content content-primary"
            #     .find("div") \  id={drug name} (approximately
            #     .findall("div")   1st: class="interactions" (dealt with above)
            interact_divs = tree_interactlist \
                .find("body") \
                .findall("div")[5] \
                .findall("div")[1] \
                .find("div") \
                .findall("div")

            for interact_div in interact_divs:
                if interact_div.attrib["class"] == "interaction-list-specific":
                    add_belongs(interact_div)
